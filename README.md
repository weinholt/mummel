# mummel

This is a Mumble protocol client.

Quick-and-dirty, no library as such yet. There is a command-line tool
that updates channel descriptions.

## Protocol documentation

For reference: https://github.com/mumble-voip/mumble-protocol

## License

GNU GPL version 3.0 or later.
