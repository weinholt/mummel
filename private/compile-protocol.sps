#!/usr/bin/env scheme-script

(import
  (protobuf compile)
  (chezscheme))

(define proto (protoc:read-proto (open-input-file "Mumble.proto")))

(let-values ((lib* (protoc:generate-libraries proto)))
  (display ";; Generated from Mumble.proto by compile-protocol.sps\n")
  (for-each pretty-print lib*))
