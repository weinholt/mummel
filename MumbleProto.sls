(library (MumbleProto SuggestConfig)
  (export SuggestConfig-builder-version
   set-SuggestConfig-builder-version!
   has-SuggestConfig-builder-version?
   clear-SuggestConfig-builder-version!
   SuggestConfig-builder-positional
   set-SuggestConfig-builder-positional!
   has-SuggestConfig-builder-positional?
   clear-SuggestConfig-builder-positional!
   SuggestConfig-builder-push_to_talk
   set-SuggestConfig-builder-push_to_talk!
   has-SuggestConfig-builder-push_to_talk?
   clear-SuggestConfig-builder-push_to_talk!
   clear-SuggestConfig-builder-extension!
   has-SuggestConfig-builder-extension?
   set-SuggestConfig-builder-extension!
   SuggestConfig-builder-extension SuggestConfig-builder-build
   SuggestConfig-builder? make-SuggestConfig-builder
   SuggestConfig-version has-SuggestConfig-version?
   SuggestConfig-positional has-SuggestConfig-positional?
   SuggestConfig-push_to_talk has-SuggestConfig-push_to_talk?
   has-SuggestConfig-extension? SuggestConfig-extension
   SuggestConfig-read SuggestConfig-write SuggestConfig?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (SuggestConfig-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-SuggestConfig-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-SuggestConfig-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-SuggestConfig-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (SuggestConfig-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-SuggestConfig-builder-version! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (SuggestConfig-builder-version-set! b0.0 0))
  (define (has-SuggestConfig-builder-version? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-SuggestConfig-builder-version! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (SuggestConfig-builder-version-set! b0.0 b1.1))
  (define (clear-SuggestConfig-builder-positional! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (SuggestConfig-builder-positional-set! b0.0 #f))
  (define (has-SuggestConfig-builder-positional? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-SuggestConfig-builder-positional! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (SuggestConfig-builder-positional-set! b0.0 b1.1))
  (define (clear-SuggestConfig-builder-push_to_talk! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (SuggestConfig-builder-push_to_talk-set! b0.0 #f))
  (define (has-SuggestConfig-builder-push_to_talk? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-SuggestConfig-builder-push_to_talk! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (SuggestConfig-builder-push_to_talk-set! b0.0 b1.1))
  (define-record-type (SuggestConfig-builder
                        make-SuggestConfig-builder
                        SuggestConfig-builder?)
    (fields
      (mutable
        version
        SuggestConfig-builder-version
        SuggestConfig-builder-version-set!)
      (mutable
        positional
        SuggestConfig-builder-positional
        SuggestConfig-builder-positional-set!)
      (mutable
        push_to_talk
        SuggestConfig-builder-push_to_talk
        SuggestConfig-builder-push_to_talk-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "version"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "positional"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 3 "push_to_talk"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor SuggestConfig)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (SuggestConfig-read r0.4)
    (protobuf:message-read (make-SuggestConfig-builder) r0.4))
  (define (SuggestConfig-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-SuggestConfig-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor SuggestConfig)
      e1.1))
  (define (SuggestConfig-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor SuggestConfig)
      e1.1))
  (define (has-SuggestConfig-version? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-SuggestConfig-positional? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-SuggestConfig-push_to_talk? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define-record-type SuggestConfig
    (fields
      (immutable version SuggestConfig-version)
      (immutable positional SuggestConfig-positional)
      (immutable push_to_talk SuggestConfig-push_to_talk))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto ServerConfig)
  (export ServerConfig-builder-max_bandwidth
   set-ServerConfig-builder-max_bandwidth!
   has-ServerConfig-builder-max_bandwidth?
   clear-ServerConfig-builder-max_bandwidth!
   ServerConfig-builder-welcome_text
   set-ServerConfig-builder-welcome_text!
   has-ServerConfig-builder-welcome_text?
   clear-ServerConfig-builder-welcome_text!
   ServerConfig-builder-allow_html
   set-ServerConfig-builder-allow_html!
   has-ServerConfig-builder-allow_html?
   clear-ServerConfig-builder-allow_html!
   ServerConfig-builder-message_length
   set-ServerConfig-builder-message_length!
   has-ServerConfig-builder-message_length?
   clear-ServerConfig-builder-message_length!
   ServerConfig-builder-image_message_length
   set-ServerConfig-builder-image_message_length!
   has-ServerConfig-builder-image_message_length?
   clear-ServerConfig-builder-image_message_length!
   ServerConfig-builder-max_users
   set-ServerConfig-builder-max_users!
   has-ServerConfig-builder-max_users?
   clear-ServerConfig-builder-max_users!
   clear-ServerConfig-builder-extension!
   has-ServerConfig-builder-extension?
   set-ServerConfig-builder-extension!
   ServerConfig-builder-extension ServerConfig-builder-build
   ServerConfig-builder? make-ServerConfig-builder
   ServerConfig-max_bandwidth has-ServerConfig-max_bandwidth?
   ServerConfig-welcome_text has-ServerConfig-welcome_text?
   ServerConfig-allow_html has-ServerConfig-allow_html?
   ServerConfig-message_length has-ServerConfig-message_length?
   ServerConfig-image_message_length
   has-ServerConfig-image_message_length?
   ServerConfig-max_users has-ServerConfig-max_users?
   has-ServerConfig-extension? ServerConfig-extension
   ServerConfig-read ServerConfig-write ServerConfig?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (ServerConfig-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ServerConfig-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ServerConfig-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ServerConfig-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ServerConfig-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ServerConfig-builder-max_bandwidth! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ServerConfig-builder-max_bandwidth-set! b0.0 0))
  (define (has-ServerConfig-builder-max_bandwidth? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ServerConfig-builder-max_bandwidth! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ServerConfig-builder-max_bandwidth-set! b0.0 b1.1))
  (define (clear-ServerConfig-builder-welcome_text! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ServerConfig-builder-welcome_text-set! b0.0 ""))
  (define (has-ServerConfig-builder-welcome_text? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ServerConfig-builder-welcome_text! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ServerConfig-builder-welcome_text-set! b0.0 b1.1))
  (define (clear-ServerConfig-builder-allow_html! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ServerConfig-builder-allow_html-set! b0.0 #f))
  (define (has-ServerConfig-builder-allow_html? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-ServerConfig-builder-allow_html! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ServerConfig-builder-allow_html-set! b0.0 b1.1))
  (define (clear-ServerConfig-builder-message_length! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (ServerConfig-builder-message_length-set! b0.0 0))
  (define (has-ServerConfig-builder-message_length? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-ServerConfig-builder-message_length! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (ServerConfig-builder-message_length-set! b0.0 b1.1))
  (define (clear-ServerConfig-builder-image_message_length!
           b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (ServerConfig-builder-image_message_length-set! b0.0 0))
  (define (has-ServerConfig-builder-image_message_length?
           b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-ServerConfig-builder-image_message_length! b0.0
           b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (ServerConfig-builder-image_message_length-set! b0.0 b1.1))
  (define (clear-ServerConfig-builder-max_users! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (ServerConfig-builder-max_users-set! b0.0 0))
  (define (has-ServerConfig-builder-max_users? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 6)))
  (define (set-ServerConfig-builder-max_users! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (ServerConfig-builder-max_users-set! b0.0 b1.1))
  (define-record-type (ServerConfig-builder
                        make-ServerConfig-builder
                        ServerConfig-builder?)
    (fields
      (mutable
        max_bandwidth
        ServerConfig-builder-max_bandwidth
        ServerConfig-builder-max_bandwidth-set!)
      (mutable
        welcome_text
        ServerConfig-builder-welcome_text
        ServerConfig-builder-welcome_text-set!)
      (mutable
        allow_html
        ServerConfig-builder-allow_html
        ServerConfig-builder-allow_html-set!)
      (mutable
        message_length
        ServerConfig-builder-message_length
        ServerConfig-builder-message_length-set!)
      (mutable
        image_message_length
        ServerConfig-builder-image_message_length
        ServerConfig-builder-image_message_length-set!)
      (mutable
        max_users
        ServerConfig-builder-max_users
        ServerConfig-builder-max_users-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "max_bandwidth"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "welcome_text"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 3 "allow_html"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 4 "message_length"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 5 "image_message_length"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 6 "max_users"
                protobuf:field-type-uint32 #f #f 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ServerConfig)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ServerConfig-read r0.4)
    (protobuf:message-read (make-ServerConfig-builder) r0.4))
  (define (ServerConfig-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ServerConfig-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ServerConfig)
      e1.1))
  (define (ServerConfig-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ServerConfig)
      e1.1))
  (define (has-ServerConfig-max_bandwidth? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-ServerConfig-welcome_text? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-ServerConfig-allow_html? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-ServerConfig-message_length? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-ServerConfig-image_message_length? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-ServerConfig-max_users? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 6)))
  (define-record-type ServerConfig
    (fields (immutable max_bandwidth ServerConfig-max_bandwidth)
      (immutable welcome_text ServerConfig-welcome_text)
      (immutable allow_html ServerConfig-allow_html)
      (immutable message_length ServerConfig-message_length)
      (immutable
        image_message_length
        ServerConfig-image_message_length)
      (immutable max_users ServerConfig-max_users))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto RequestBlob)
  (export RequestBlob-builder-session_texture
   set-RequestBlob-builder-session_texture!
   clear-RequestBlob-builder-session_texture!
   RequestBlob-builder-session_comment
   set-RequestBlob-builder-session_comment!
   clear-RequestBlob-builder-session_comment!
   RequestBlob-builder-channel_description
   set-RequestBlob-builder-channel_description!
   clear-RequestBlob-builder-channel_description!
   clear-RequestBlob-builder-extension!
   has-RequestBlob-builder-extension?
   set-RequestBlob-builder-extension!
   RequestBlob-builder-extension RequestBlob-builder-build
   RequestBlob-builder? make-RequestBlob-builder
   RequestBlob-session_texture RequestBlob-session_comment
   RequestBlob-channel_description has-RequestBlob-extension?
   RequestBlob-extension RequestBlob-read RequestBlob-write
   RequestBlob?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (RequestBlob-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-RequestBlob-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-RequestBlob-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-RequestBlob-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (RequestBlob-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-RequestBlob-builder-session_texture! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (RequestBlob-builder-session_texture-set! b0.0 '()))
  (define (set-RequestBlob-builder-session_texture! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (RequestBlob-builder-session_texture-set! b0.0 b1.1))
  (define (clear-RequestBlob-builder-session_comment! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (RequestBlob-builder-session_comment-set! b0.0 '()))
  (define (set-RequestBlob-builder-session_comment! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (RequestBlob-builder-session_comment-set! b0.0 b1.1))
  (define (clear-RequestBlob-builder-channel_description!
           b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (RequestBlob-builder-channel_description-set! b0.0 '()))
  (define (set-RequestBlob-builder-channel_description! b0.0
           b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (RequestBlob-builder-channel_description-set! b0.0 b1.1))
  (define-record-type (RequestBlob-builder
                        make-RequestBlob-builder
                        RequestBlob-builder?)
    (fields
      (mutable
        session_texture
        RequestBlob-builder-session_texture
        RequestBlob-builder-session_texture-set!)
      (mutable
        session_comment
        RequestBlob-builder-session_comment
        RequestBlob-builder-session_comment-set!)
      (mutable
        channel_description
        RequestBlob-builder-channel_description
        RequestBlob-builder-channel_description-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "session_texture"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 2 "session_comment"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 3 "channel_description"
                protobuf:field-type-uint32 #t #f '())))
          (let ([b3.3 (b1.1
                        (record-type-descriptor RequestBlob)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (RequestBlob-read r0.4)
    (protobuf:message-read (make-RequestBlob-builder) r0.4))
  (define (RequestBlob-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-RequestBlob-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor RequestBlob)
      e1.1))
  (define (RequestBlob-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor RequestBlob)
      e1.1))
  (define-record-type RequestBlob
    (fields
      (immutable session_texture RequestBlob-session_texture)
      (immutable session_comment RequestBlob-session_comment)
      (immutable
        channel_description
        RequestBlob-channel_description))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto UserStats)
  (export UserStats-Stats-builder-good
   set-UserStats-Stats-builder-good!
   has-UserStats-Stats-builder-good?
   clear-UserStats-Stats-builder-good!
   UserStats-Stats-builder-late
   set-UserStats-Stats-builder-late!
   has-UserStats-Stats-builder-late?
   clear-UserStats-Stats-builder-late!
   UserStats-Stats-builder-lost
   set-UserStats-Stats-builder-lost!
   has-UserStats-Stats-builder-lost?
   clear-UserStats-Stats-builder-lost!
   UserStats-Stats-builder-resync
   set-UserStats-Stats-builder-resync!
   has-UserStats-Stats-builder-resync?
   clear-UserStats-Stats-builder-resync!
   clear-UserStats-Stats-builder-extension!
   has-UserStats-Stats-builder-extension?
   set-UserStats-Stats-builder-extension!
   UserStats-Stats-builder-extension
   UserStats-Stats-builder-build UserStats-Stats-builder?
   make-UserStats-Stats-builder UserStats-Stats-good
   has-UserStats-Stats-good? UserStats-Stats-late
   has-UserStats-Stats-late? UserStats-Stats-lost
   has-UserStats-Stats-lost? UserStats-Stats-resync
   has-UserStats-Stats-resync? has-UserStats-Stats-extension?
   UserStats-Stats-extension UserStats-Stats-read
   UserStats-Stats-write UserStats-Stats?
   UserStats-builder-session set-UserStats-builder-session!
   has-UserStats-builder-session?
   clear-UserStats-builder-session!
   UserStats-builder-stats_only
   set-UserStats-builder-stats_only!
   has-UserStats-builder-stats_only?
   clear-UserStats-builder-stats_only!
   UserStats-builder-certificates
   set-UserStats-builder-certificates!
   clear-UserStats-builder-certificates!
   UserStats-builder-from_client
   set-UserStats-builder-from_client!
   has-UserStats-builder-from_client?
   clear-UserStats-builder-from_client!
   UserStats-builder-from_server
   set-UserStats-builder-from_server!
   has-UserStats-builder-from_server?
   clear-UserStats-builder-from_server!
   UserStats-builder-udp_packets
   set-UserStats-builder-udp_packets!
   has-UserStats-builder-udp_packets?
   clear-UserStats-builder-udp_packets!
   UserStats-builder-tcp_packets
   set-UserStats-builder-tcp_packets!
   has-UserStats-builder-tcp_packets?
   clear-UserStats-builder-tcp_packets!
   UserStats-builder-udp_ping_avg
   set-UserStats-builder-udp_ping_avg!
   has-UserStats-builder-udp_ping_avg?
   clear-UserStats-builder-udp_ping_avg!
   UserStats-builder-udp_ping_var
   set-UserStats-builder-udp_ping_var!
   has-UserStats-builder-udp_ping_var?
   clear-UserStats-builder-udp_ping_var!
   UserStats-builder-tcp_ping_avg
   set-UserStats-builder-tcp_ping_avg!
   has-UserStats-builder-tcp_ping_avg?
   clear-UserStats-builder-tcp_ping_avg!
   UserStats-builder-tcp_ping_var
   set-UserStats-builder-tcp_ping_var!
   has-UserStats-builder-tcp_ping_var?
   clear-UserStats-builder-tcp_ping_var!
   UserStats-builder-version set-UserStats-builder-version!
   has-UserStats-builder-version?
   clear-UserStats-builder-version!
   UserStats-builder-celt_versions
   set-UserStats-builder-celt_versions!
   clear-UserStats-builder-celt_versions!
   UserStats-builder-address set-UserStats-builder-address!
   has-UserStats-builder-address?
   clear-UserStats-builder-address! UserStats-builder-bandwidth
   set-UserStats-builder-bandwidth!
   has-UserStats-builder-bandwidth?
   clear-UserStats-builder-bandwidth!
   UserStats-builder-onlinesecs
   set-UserStats-builder-onlinesecs!
   has-UserStats-builder-onlinesecs?
   clear-UserStats-builder-onlinesecs!
   UserStats-builder-idlesecs set-UserStats-builder-idlesecs!
   has-UserStats-builder-idlesecs?
   clear-UserStats-builder-idlesecs!
   UserStats-builder-strong_certificate
   set-UserStats-builder-strong_certificate!
   has-UserStats-builder-strong_certificate?
   clear-UserStats-builder-strong_certificate!
   UserStats-builder-opus set-UserStats-builder-opus!
   has-UserStats-builder-opus? clear-UserStats-builder-opus!
   clear-UserStats-builder-extension!
   has-UserStats-builder-extension?
   set-UserStats-builder-extension! UserStats-builder-extension
   UserStats-builder-build UserStats-builder?
   make-UserStats-builder UserStats-session
   has-UserStats-session? UserStats-stats_only
   has-UserStats-stats_only? UserStats-certificates
   UserStats-from_client has-UserStats-from_client?
   UserStats-from_server has-UserStats-from_server?
   UserStats-udp_packets has-UserStats-udp_packets?
   UserStats-tcp_packets has-UserStats-tcp_packets?
   UserStats-udp_ping_avg has-UserStats-udp_ping_avg?
   UserStats-udp_ping_var has-UserStats-udp_ping_var?
   UserStats-tcp_ping_avg has-UserStats-tcp_ping_avg?
   UserStats-tcp_ping_var has-UserStats-tcp_ping_var?
   UserStats-version has-UserStats-version?
   UserStats-celt_versions UserStats-address
   has-UserStats-address? UserStats-bandwidth
   has-UserStats-bandwidth? UserStats-onlinesecs
   has-UserStats-onlinesecs? UserStats-idlesecs
   has-UserStats-idlesecs? UserStats-strong_certificate
   has-UserStats-strong_certificate? UserStats-opus
   has-UserStats-opus? has-UserStats-extension?
   UserStats-extension UserStats-read UserStats-write
   UserStats?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private)
    (MumbleProto Version) (MumbleProto Version))
  (define (UserStats-Stats-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-UserStats-Stats-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-UserStats-Stats-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-UserStats-Stats-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (UserStats-Stats-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-UserStats-Stats-builder-good! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (UserStats-Stats-builder-good-set! b0.0 0))
  (define (has-UserStats-Stats-builder-good? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-UserStats-Stats-builder-good! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (UserStats-Stats-builder-good-set! b0.0 b1.1))
  (define (clear-UserStats-Stats-builder-late! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (UserStats-Stats-builder-late-set! b0.0 0))
  (define (has-UserStats-Stats-builder-late? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-UserStats-Stats-builder-late! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (UserStats-Stats-builder-late-set! b0.0 b1.1))
  (define (clear-UserStats-Stats-builder-lost! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (UserStats-Stats-builder-lost-set! b0.0 0))
  (define (has-UserStats-Stats-builder-lost? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-UserStats-Stats-builder-lost! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (UserStats-Stats-builder-lost-set! b0.0 b1.1))
  (define (clear-UserStats-Stats-builder-resync! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (UserStats-Stats-builder-resync-set! b0.0 0))
  (define (has-UserStats-Stats-builder-resync? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-UserStats-Stats-builder-resync! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (UserStats-Stats-builder-resync-set! b0.0 b1.1))
  (define-record-type (UserStats-Stats-builder
                        make-UserStats-Stats-builder
                        UserStats-Stats-builder?)
    (fields
      (mutable
        good
        UserStats-Stats-builder-good
        UserStats-Stats-builder-good-set!)
      (mutable
        late
        UserStats-Stats-builder-late
        UserStats-Stats-builder-late-set!)
      (mutable
        lost
        UserStats-Stats-builder-lost
        UserStats-Stats-builder-lost-set!)
      (mutable
        resync
        UserStats-Stats-builder-resync
        UserStats-Stats-builder-resync-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "good"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "late"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "lost"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 4 "resync"
                protobuf:field-type-uint32 #f #f 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor UserStats-Stats)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (UserStats-Stats-read r0.4)
    (protobuf:message-read (make-UserStats-Stats-builder) r0.4))
  (define (UserStats-Stats-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-UserStats-Stats-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor UserStats-Stats)
      e1.1))
  (define (UserStats-Stats-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor UserStats-Stats)
      e1.1))
  (define (has-UserStats-Stats-good? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-UserStats-Stats-late? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-UserStats-Stats-lost? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-UserStats-Stats-resync? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type UserStats-Stats
    (fields
      (immutable good UserStats-Stats-good)
      (immutable late UserStats-Stats-late)
      (immutable lost UserStats-Stats-lost)
      (immutable resync UserStats-Stats-resync))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t))
  (define (UserStats-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-UserStats-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-UserStats-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-UserStats-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (UserStats-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-UserStats-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (UserStats-builder-session-set! b0.0 0))
  (define (has-UserStats-builder-session? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-UserStats-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (UserStats-builder-session-set! b0.0 b1.1))
  (define (clear-UserStats-builder-stats_only! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (UserStats-builder-stats_only-set! b0.0 #f))
  (define (has-UserStats-builder-stats_only? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-UserStats-builder-stats_only! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (UserStats-builder-stats_only-set! b0.0 b1.1))
  (define (clear-UserStats-builder-certificates! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (UserStats-builder-certificates-set! b0.0 '()))
  (define (set-UserStats-builder-certificates! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (UserStats-builder-certificates-set! b0.0 b1.1))
  (define (clear-UserStats-builder-from_client! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (UserStats-builder-from_client-set! b0.0 #f))
  (define (has-UserStats-builder-from_client? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-UserStats-builder-from_client! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (UserStats-builder-from_client-set! b0.0 b1.1))
  (define (clear-UserStats-builder-from_server! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (UserStats-builder-from_server-set! b0.0 #f))
  (define (has-UserStats-builder-from_server? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-UserStats-builder-from_server! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (UserStats-builder-from_server-set! b0.0 b1.1))
  (define (clear-UserStats-builder-udp_packets! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (UserStats-builder-udp_packets-set! b0.0 0))
  (define (has-UserStats-builder-udp_packets? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 6)))
  (define (set-UserStats-builder-udp_packets! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (UserStats-builder-udp_packets-set! b0.0 b1.1))
  (define (clear-UserStats-builder-tcp_packets! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 7))
    (UserStats-builder-tcp_packets-set! b0.0 0))
  (define (has-UserStats-builder-tcp_packets? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 7)))
  (define (set-UserStats-builder-tcp_packets! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 7)
      b1.1)
    (UserStats-builder-tcp_packets-set! b0.0 b1.1))
  (define (clear-UserStats-builder-udp_ping_avg! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 8))
    (UserStats-builder-udp_ping_avg-set! b0.0 0))
  (define (has-UserStats-builder-udp_ping_avg? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 8)))
  (define (set-UserStats-builder-udp_ping_avg! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 8)
      b1.1)
    (UserStats-builder-udp_ping_avg-set! b0.0 b1.1))
  (define (clear-UserStats-builder-udp_ping_var! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 9))
    (UserStats-builder-udp_ping_var-set! b0.0 0))
  (define (has-UserStats-builder-udp_ping_var? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 9)))
  (define (set-UserStats-builder-udp_ping_var! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 9)
      b1.1)
    (UserStats-builder-udp_ping_var-set! b0.0 b1.1))
  (define (clear-UserStats-builder-tcp_ping_avg! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 10))
    (UserStats-builder-tcp_ping_avg-set! b0.0 0))
  (define (has-UserStats-builder-tcp_ping_avg? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 10)))
  (define (set-UserStats-builder-tcp_ping_avg! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 10)
      b1.1)
    (UserStats-builder-tcp_ping_avg-set! b0.0 b1.1))
  (define (clear-UserStats-builder-tcp_ping_var! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 11))
    (UserStats-builder-tcp_ping_var-set! b0.0 0))
  (define (has-UserStats-builder-tcp_ping_var? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 11)))
  (define (set-UserStats-builder-tcp_ping_var! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 11)
      b1.1)
    (UserStats-builder-tcp_ping_var-set! b0.0 b1.1))
  (define (clear-UserStats-builder-version! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 12))
    (UserStats-builder-version-set! b0.0 #f))
  (define (has-UserStats-builder-version? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 12)))
  (define (set-UserStats-builder-version! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 12)
      b1.1)
    (UserStats-builder-version-set! b0.0 b1.1))
  (define (clear-UserStats-builder-celt_versions! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 13))
    (UserStats-builder-celt_versions-set! b0.0 '()))
  (define (set-UserStats-builder-celt_versions! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 13)
      b1.1)
    (UserStats-builder-celt_versions-set! b0.0 b1.1))
  (define (clear-UserStats-builder-address! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 14))
    (UserStats-builder-address-set! b0.0 #vu8()))
  (define (has-UserStats-builder-address? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 14)))
  (define (set-UserStats-builder-address! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 14)
      b1.1)
    (UserStats-builder-address-set! b0.0 b1.1))
  (define (clear-UserStats-builder-bandwidth! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 15))
    (UserStats-builder-bandwidth-set! b0.0 0))
  (define (has-UserStats-builder-bandwidth? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 15)))
  (define (set-UserStats-builder-bandwidth! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 15)
      b1.1)
    (UserStats-builder-bandwidth-set! b0.0 b1.1))
  (define (clear-UserStats-builder-onlinesecs! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 16))
    (UserStats-builder-onlinesecs-set! b0.0 0))
  (define (has-UserStats-builder-onlinesecs? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 16)))
  (define (set-UserStats-builder-onlinesecs! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 16)
      b1.1)
    (UserStats-builder-onlinesecs-set! b0.0 b1.1))
  (define (clear-UserStats-builder-idlesecs! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 17))
    (UserStats-builder-idlesecs-set! b0.0 0))
  (define (has-UserStats-builder-idlesecs? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 17)))
  (define (set-UserStats-builder-idlesecs! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 17)
      b1.1)
    (UserStats-builder-idlesecs-set! b0.0 b1.1))
  (define (clear-UserStats-builder-strong_certificate! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 18))
    (UserStats-builder-strong_certificate-set! b0.0 #f))
  (define (has-UserStats-builder-strong_certificate? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 18)))
  (define (set-UserStats-builder-strong_certificate! b0.0
           b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 18)
      b1.1)
    (UserStats-builder-strong_certificate-set! b0.0 b1.1))
  (define (clear-UserStats-builder-opus! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 19))
    (UserStats-builder-opus-set! b0.0 #f))
  (define (has-UserStats-builder-opus? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 19)))
  (define (set-UserStats-builder-opus! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 19)
      b1.1)
    (UserStats-builder-opus-set! b0.0 b1.1))
  (define-record-type (UserStats-builder
                        make-UserStats-builder
                        UserStats-builder?)
    (fields
      (mutable
        session
        UserStats-builder-session
        UserStats-builder-session-set!)
      (mutable
        stats_only
        UserStats-builder-stats_only
        UserStats-builder-stats_only-set!)
      (mutable
        certificates
        UserStats-builder-certificates
        UserStats-builder-certificates-set!)
      (mutable
        from_client
        UserStats-builder-from_client
        UserStats-builder-from_client-set!)
      (mutable
        from_server
        UserStats-builder-from_server
        UserStats-builder-from_server-set!)
      (mutable
        udp_packets
        UserStats-builder-udp_packets
        UserStats-builder-udp_packets-set!)
      (mutable
        tcp_packets
        UserStats-builder-tcp_packets
        UserStats-builder-tcp_packets-set!)
      (mutable
        udp_ping_avg
        UserStats-builder-udp_ping_avg
        UserStats-builder-udp_ping_avg-set!)
      (mutable
        udp_ping_var
        UserStats-builder-udp_ping_var
        UserStats-builder-udp_ping_var-set!)
      (mutable
        tcp_ping_avg
        UserStats-builder-tcp_ping_avg
        UserStats-builder-tcp_ping_avg-set!)
      (mutable
        tcp_ping_var
        UserStats-builder-tcp_ping_var
        UserStats-builder-tcp_ping_var-set!)
      (mutable
        version
        UserStats-builder-version
        UserStats-builder-version-set!)
      (mutable
        celt_versions
        UserStats-builder-celt_versions
        UserStats-builder-celt_versions-set!)
      (mutable
        address
        UserStats-builder-address
        UserStats-builder-address-set!)
      (mutable
        bandwidth
        UserStats-builder-bandwidth
        UserStats-builder-bandwidth-set!)
      (mutable
        onlinesecs
        UserStats-builder-onlinesecs
        UserStats-builder-onlinesecs-set!)
      (mutable
        idlesecs
        UserStats-builder-idlesecs
        UserStats-builder-idlesecs-set!)
      (mutable
        strong_certificate
        UserStats-builder-strong_certificate
        UserStats-builder-strong_certificate-set!)
      (mutable
        opus
        UserStats-builder-opus
        UserStats-builder-opus-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "session"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "stats_only"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 3 "certificates"
                protobuf:field-type-bytes #t #f '())
              (protobuf:make-field-descriptor 4 "from_client"
                (protobuf:make-message-field-type-descriptor "Stats" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-UserStats-Stats-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  UserStats-Stats? #f #f)
                #f #f #f)
              (protobuf:make-field-descriptor 5 "from_server"
                (protobuf:make-message-field-type-descriptor "Stats" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-UserStats-Stats-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  UserStats-Stats? #f #f)
                #f #f #f)
              (protobuf:make-field-descriptor 6 "udp_packets"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 7 "tcp_packets"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 8 "udp_ping_avg"
                protobuf:field-type-float #f #f 0)
              (protobuf:make-field-descriptor 9 "udp_ping_var"
                protobuf:field-type-float #f #f 0)
              (protobuf:make-field-descriptor 10 "tcp_ping_avg"
                protobuf:field-type-float #f #f 0)
              (protobuf:make-field-descriptor 11 "tcp_ping_var"
                protobuf:field-type-float #f #f 0)
              (protobuf:make-field-descriptor 12 "version"
                (protobuf:make-message-field-type-descriptor "Version" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-Version-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  Version? #f #f)
                #f #f #f)
              (protobuf:make-field-descriptor 13 "celt_versions"
                protobuf:field-type-int32 #t #f '())
              (protobuf:make-field-descriptor 14 "address"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 15 "bandwidth"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 16 "onlinesecs"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 17 "idlesecs"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 18 "strong_certificate"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 19 "opus"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1 (record-type-descriptor UserStats) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (UserStats-read r0.4)
    (protobuf:message-read (make-UserStats-builder) r0.4))
  (define (UserStats-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-UserStats-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor UserStats)
      e1.1))
  (define (UserStats-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor UserStats)
      e1.1))
  (define (has-UserStats-session? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-UserStats-stats_only? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-UserStats-from_client? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-UserStats-from_server? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-UserStats-udp_packets? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 6)))
  (define (has-UserStats-tcp_packets? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 7)))
  (define (has-UserStats-udp_ping_avg? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 8)))
  (define (has-UserStats-udp_ping_var? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 9)))
  (define (has-UserStats-tcp_ping_avg? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 10)))
  (define (has-UserStats-tcp_ping_var? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 11)))
  (define (has-UserStats-version? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 12)))
  (define (has-UserStats-address? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 14)))
  (define (has-UserStats-bandwidth? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 15)))
  (define (has-UserStats-onlinesecs? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 16)))
  (define (has-UserStats-idlesecs? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 17)))
  (define (has-UserStats-strong_certificate? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 18)))
  (define (has-UserStats-opus? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 19)))
  (define-record-type UserStats
    (fields (immutable session UserStats-session)
      (immutable stats_only UserStats-stats_only)
      (immutable certificates UserStats-certificates)
      (immutable from_client UserStats-from_client)
      (immutable from_server UserStats-from_server)
      (immutable udp_packets UserStats-udp_packets)
      (immutable tcp_packets UserStats-tcp_packets)
      (immutable udp_ping_avg UserStats-udp_ping_avg)
      (immutable udp_ping_var UserStats-udp_ping_var)
      (immutable tcp_ping_avg UserStats-tcp_ping_avg)
      (immutable tcp_ping_var UserStats-tcp_ping_var)
      (immutable version UserStats-version)
      (immutable celt_versions UserStats-celt_versions)
      (immutable address UserStats-address)
      (immutable bandwidth UserStats-bandwidth)
      (immutable onlinesecs UserStats-onlinesecs)
      (immutable idlesecs UserStats-idlesecs)
      (immutable strong_certificate UserStats-strong_certificate)
      (immutable opus UserStats-opus))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto CodecVersion)
  (export CodecVersion-builder-alpha
   set-CodecVersion-builder-alpha!
   has-CodecVersion-builder-alpha?
   clear-CodecVersion-builder-alpha! CodecVersion-builder-beta
   set-CodecVersion-builder-beta!
   has-CodecVersion-builder-beta?
   clear-CodecVersion-builder-beta!
   CodecVersion-builder-prefer_alpha
   set-CodecVersion-builder-prefer_alpha!
   has-CodecVersion-builder-prefer_alpha?
   clear-CodecVersion-builder-prefer_alpha!
   CodecVersion-builder-opus set-CodecVersion-builder-opus!
   has-CodecVersion-builder-opus?
   clear-CodecVersion-builder-opus!
   clear-CodecVersion-builder-extension!
   has-CodecVersion-builder-extension?
   set-CodecVersion-builder-extension!
   CodecVersion-builder-extension CodecVersion-builder-build
   CodecVersion-builder? make-CodecVersion-builder
   CodecVersion-alpha CodecVersion-beta
   CodecVersion-prefer_alpha CodecVersion-opus
   has-CodecVersion-opus? has-CodecVersion-extension?
   CodecVersion-extension CodecVersion-read CodecVersion-write
   CodecVersion?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (CodecVersion-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-CodecVersion-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-CodecVersion-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-CodecVersion-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (CodecVersion-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-CodecVersion-builder-alpha! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (CodecVersion-builder-alpha-set! b0.0 0))
  (define (has-CodecVersion-builder-alpha? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-CodecVersion-builder-alpha! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (CodecVersion-builder-alpha-set! b0.0 b1.1))
  (define (clear-CodecVersion-builder-beta! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (CodecVersion-builder-beta-set! b0.0 0))
  (define (has-CodecVersion-builder-beta? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-CodecVersion-builder-beta! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (CodecVersion-builder-beta-set! b0.0 b1.1))
  (define (clear-CodecVersion-builder-prefer_alpha! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (CodecVersion-builder-prefer_alpha-set! b0.0 #t))
  (define (has-CodecVersion-builder-prefer_alpha? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-CodecVersion-builder-prefer_alpha! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (CodecVersion-builder-prefer_alpha-set! b0.0 b1.1))
  (define (clear-CodecVersion-builder-opus! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (CodecVersion-builder-opus-set! b0.0 #f))
  (define (has-CodecVersion-builder-opus? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-CodecVersion-builder-opus! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (CodecVersion-builder-opus-set! b0.0 b1.1))
  (define-record-type (CodecVersion-builder
                        make-CodecVersion-builder
                        CodecVersion-builder?)
    (fields
      (mutable
        alpha
        CodecVersion-builder-alpha
        CodecVersion-builder-alpha-set!)
      (mutable
        beta
        CodecVersion-builder-beta
        CodecVersion-builder-beta-set!)
      (mutable
        prefer_alpha
        CodecVersion-builder-prefer_alpha
        CodecVersion-builder-prefer_alpha-set!)
      (mutable
        opus
        CodecVersion-builder-opus
        CodecVersion-builder-opus-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "alpha"
                protobuf:field-type-int32 #f #t 0)
              (protobuf:make-field-descriptor 2 "beta"
                protobuf:field-type-int32 #f #t 0)
              (protobuf:make-field-descriptor 3 "prefer_alpha"
                protobuf:field-type-bool #f #t #t)
              (protobuf:make-field-descriptor 4 "opus"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor CodecVersion)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (CodecVersion-read r0.4)
    (protobuf:message-read (make-CodecVersion-builder) r0.4))
  (define (CodecVersion-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-CodecVersion-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor CodecVersion)
      e1.1))
  (define (CodecVersion-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor CodecVersion)
      e1.1))
  (define (has-CodecVersion-opus? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type CodecVersion
    (fields
      (immutable alpha CodecVersion-alpha)
      (immutable beta CodecVersion-beta)
      (immutable prefer_alpha CodecVersion-prefer_alpha)
      (immutable opus CodecVersion-opus))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto PermissionQuery)
  (export PermissionQuery-builder-channel_id
   set-PermissionQuery-builder-channel_id!
   has-PermissionQuery-builder-channel_id?
   clear-PermissionQuery-builder-channel_id!
   PermissionQuery-builder-permissions
   set-PermissionQuery-builder-permissions!
   has-PermissionQuery-builder-permissions?
   clear-PermissionQuery-builder-permissions!
   PermissionQuery-builder-flush
   set-PermissionQuery-builder-flush!
   has-PermissionQuery-builder-flush?
   clear-PermissionQuery-builder-flush!
   clear-PermissionQuery-builder-extension!
   has-PermissionQuery-builder-extension?
   set-PermissionQuery-builder-extension!
   PermissionQuery-builder-extension
   PermissionQuery-builder-build PermissionQuery-builder?
   make-PermissionQuery-builder PermissionQuery-channel_id
   has-PermissionQuery-channel_id? PermissionQuery-permissions
   has-PermissionQuery-permissions? PermissionQuery-flush
   has-PermissionQuery-flush? has-PermissionQuery-extension?
   PermissionQuery-extension PermissionQuery-read
   PermissionQuery-write PermissionQuery?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (PermissionQuery-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-PermissionQuery-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-PermissionQuery-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-PermissionQuery-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (PermissionQuery-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-PermissionQuery-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (PermissionQuery-builder-channel_id-set! b0.0 0))
  (define (has-PermissionQuery-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-PermissionQuery-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (PermissionQuery-builder-channel_id-set! b0.0 b1.1))
  (define (clear-PermissionQuery-builder-permissions! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (PermissionQuery-builder-permissions-set! b0.0 0))
  (define (has-PermissionQuery-builder-permissions? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-PermissionQuery-builder-permissions! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (PermissionQuery-builder-permissions-set! b0.0 b1.1))
  (define (clear-PermissionQuery-builder-flush! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (PermissionQuery-builder-flush-set! b0.0 #f))
  (define (has-PermissionQuery-builder-flush? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-PermissionQuery-builder-flush! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (PermissionQuery-builder-flush-set! b0.0 b1.1))
  (define-record-type (PermissionQuery-builder
                        make-PermissionQuery-builder
                        PermissionQuery-builder?)
    (fields
      (mutable
        channel_id
        PermissionQuery-builder-channel_id
        PermissionQuery-builder-channel_id-set!)
      (mutable
        permissions
        PermissionQuery-builder-permissions
        PermissionQuery-builder-permissions-set!)
      (mutable
        flush
        PermissionQuery-builder-flush
        PermissionQuery-builder-flush-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "channel_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "permissions"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "flush"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor PermissionQuery)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (PermissionQuery-read r0.4)
    (protobuf:message-read (make-PermissionQuery-builder) r0.4))
  (define (PermissionQuery-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-PermissionQuery-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor PermissionQuery)
      e1.1))
  (define (PermissionQuery-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor PermissionQuery)
      e1.1))
  (define (has-PermissionQuery-channel_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-PermissionQuery-permissions? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-PermissionQuery-flush? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define-record-type PermissionQuery
    (fields
      (immutable channel_id PermissionQuery-channel_id)
      (immutable permissions PermissionQuery-permissions)
      (immutable flush PermissionQuery-flush))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto VoiceTarget)
  (export VoiceTarget-Target-builder-session
   set-VoiceTarget-Target-builder-session!
   clear-VoiceTarget-Target-builder-session!
   VoiceTarget-Target-builder-channel_id
   set-VoiceTarget-Target-builder-channel_id!
   has-VoiceTarget-Target-builder-channel_id?
   clear-VoiceTarget-Target-builder-channel_id!
   VoiceTarget-Target-builder-group
   set-VoiceTarget-Target-builder-group!
   has-VoiceTarget-Target-builder-group?
   clear-VoiceTarget-Target-builder-group!
   VoiceTarget-Target-builder-links
   set-VoiceTarget-Target-builder-links!
   has-VoiceTarget-Target-builder-links?
   clear-VoiceTarget-Target-builder-links!
   VoiceTarget-Target-builder-children
   set-VoiceTarget-Target-builder-children!
   has-VoiceTarget-Target-builder-children?
   clear-VoiceTarget-Target-builder-children!
   clear-VoiceTarget-Target-builder-extension!
   has-VoiceTarget-Target-builder-extension?
   set-VoiceTarget-Target-builder-extension!
   VoiceTarget-Target-builder-extension
   VoiceTarget-Target-builder-build VoiceTarget-Target-builder?
   make-VoiceTarget-Target-builder VoiceTarget-Target-session
   VoiceTarget-Target-channel_id
   has-VoiceTarget-Target-channel_id? VoiceTarget-Target-group
   has-VoiceTarget-Target-group? VoiceTarget-Target-links
   has-VoiceTarget-Target-links? VoiceTarget-Target-children
   has-VoiceTarget-Target-children?
   has-VoiceTarget-Target-extension?
   VoiceTarget-Target-extension VoiceTarget-Target-read
   VoiceTarget-Target-write VoiceTarget-Target?
   VoiceTarget-builder-id set-VoiceTarget-builder-id!
   has-VoiceTarget-builder-id? clear-VoiceTarget-builder-id!
   VoiceTarget-builder-targets set-VoiceTarget-builder-targets!
   clear-VoiceTarget-builder-targets!
   clear-VoiceTarget-builder-extension!
   has-VoiceTarget-builder-extension?
   set-VoiceTarget-builder-extension!
   VoiceTarget-builder-extension VoiceTarget-builder-build
   VoiceTarget-builder? make-VoiceTarget-builder VoiceTarget-id
   has-VoiceTarget-id? VoiceTarget-targets
   has-VoiceTarget-extension? VoiceTarget-extension
   VoiceTarget-read VoiceTarget-write VoiceTarget?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (VoiceTarget-Target-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-VoiceTarget-Target-builder-extension! b0.0
           b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-VoiceTarget-Target-builder-extension? b0.0
           b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-VoiceTarget-Target-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (VoiceTarget-Target-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-VoiceTarget-Target-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (VoiceTarget-Target-builder-session-set! b0.0 '()))
  (define (set-VoiceTarget-Target-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (VoiceTarget-Target-builder-session-set! b0.0 b1.1))
  (define (clear-VoiceTarget-Target-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (VoiceTarget-Target-builder-channel_id-set! b0.0 0))
  (define (has-VoiceTarget-Target-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-VoiceTarget-Target-builder-channel_id! b0.0
           b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (VoiceTarget-Target-builder-channel_id-set! b0.0 b1.1))
  (define (clear-VoiceTarget-Target-builder-group! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (VoiceTarget-Target-builder-group-set! b0.0 ""))
  (define (has-VoiceTarget-Target-builder-group? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-VoiceTarget-Target-builder-group! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (VoiceTarget-Target-builder-group-set! b0.0 b1.1))
  (define (clear-VoiceTarget-Target-builder-links! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (VoiceTarget-Target-builder-links-set! b0.0 #f))
  (define (has-VoiceTarget-Target-builder-links? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-VoiceTarget-Target-builder-links! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (VoiceTarget-Target-builder-links-set! b0.0 b1.1))
  (define (clear-VoiceTarget-Target-builder-children! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (VoiceTarget-Target-builder-children-set! b0.0 #f))
  (define (has-VoiceTarget-Target-builder-children? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-VoiceTarget-Target-builder-children! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (VoiceTarget-Target-builder-children-set! b0.0 b1.1))
  (define-record-type (VoiceTarget-Target-builder
                        make-VoiceTarget-Target-builder
                        VoiceTarget-Target-builder?)
    (fields
      (mutable
        session
        VoiceTarget-Target-builder-session
        VoiceTarget-Target-builder-session-set!)
      (mutable
        channel_id
        VoiceTarget-Target-builder-channel_id
        VoiceTarget-Target-builder-channel_id-set!)
      (mutable
        group
        VoiceTarget-Target-builder-group
        VoiceTarget-Target-builder-group-set!)
      (mutable
        links
        VoiceTarget-Target-builder-links
        VoiceTarget-Target-builder-links-set!)
      (mutable
        children
        VoiceTarget-Target-builder-children
        VoiceTarget-Target-builder-children-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "session"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 2 "channel_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "group"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "links"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 5 "children"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor VoiceTarget-Target)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (VoiceTarget-Target-read r0.4)
    (protobuf:message-read
      (make-VoiceTarget-Target-builder)
      r0.4))
  (define (VoiceTarget-Target-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-VoiceTarget-Target-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor VoiceTarget-Target)
      e1.1))
  (define (VoiceTarget-Target-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor VoiceTarget-Target)
      e1.1))
  (define (has-VoiceTarget-Target-channel_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-VoiceTarget-Target-group? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-VoiceTarget-Target-links? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-VoiceTarget-Target-children? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define-record-type VoiceTarget-Target
    (fields (immutable session VoiceTarget-Target-session)
      (immutable channel_id VoiceTarget-Target-channel_id)
      (immutable group VoiceTarget-Target-group)
      (immutable links VoiceTarget-Target-links)
      (immutable children VoiceTarget-Target-children))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t))
  (define (VoiceTarget-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-VoiceTarget-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-VoiceTarget-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-VoiceTarget-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (VoiceTarget-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-VoiceTarget-builder-id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (VoiceTarget-builder-id-set! b0.0 0))
  (define (has-VoiceTarget-builder-id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-VoiceTarget-builder-id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (VoiceTarget-builder-id-set! b0.0 b1.1))
  (define (clear-VoiceTarget-builder-targets! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (VoiceTarget-builder-targets-set! b0.0 '()))
  (define (set-VoiceTarget-builder-targets! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (VoiceTarget-builder-targets-set! b0.0 b1.1))
  (define-record-type (VoiceTarget-builder
                        make-VoiceTarget-builder
                        VoiceTarget-builder?)
    (fields
      (mutable
        id
        VoiceTarget-builder-id
        VoiceTarget-builder-id-set!)
      (mutable
        targets
        VoiceTarget-builder-targets
        VoiceTarget-builder-targets-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "targets"
                (protobuf:make-message-field-type-descriptor "Target" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-VoiceTarget-Target-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  VoiceTarget-Target? #f #f)
                #t #f '())))
          (let ([b3.3 (b1.1
                        (record-type-descriptor VoiceTarget)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (VoiceTarget-read r0.4)
    (protobuf:message-read (make-VoiceTarget-builder) r0.4))
  (define (VoiceTarget-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-VoiceTarget-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor VoiceTarget)
      e1.1))
  (define (VoiceTarget-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor VoiceTarget)
      e1.1))
  (define (has-VoiceTarget-id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define-record-type VoiceTarget
    (fields
      (immutable id VoiceTarget-id)
      (immutable targets VoiceTarget-targets))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto UserList)
  (export UserList-User-builder-user_id
   set-UserList-User-builder-user_id!
   has-UserList-User-builder-user_id?
   clear-UserList-User-builder-user_id!
   UserList-User-builder-name set-UserList-User-builder-name!
   has-UserList-User-builder-name?
   clear-UserList-User-builder-name!
   UserList-User-builder-last_seen
   set-UserList-User-builder-last_seen!
   has-UserList-User-builder-last_seen?
   clear-UserList-User-builder-last_seen!
   UserList-User-builder-last_channel
   set-UserList-User-builder-last_channel!
   has-UserList-User-builder-last_channel?
   clear-UserList-User-builder-last_channel!
   clear-UserList-User-builder-extension!
   has-UserList-User-builder-extension?
   set-UserList-User-builder-extension!
   UserList-User-builder-extension UserList-User-builder-build
   UserList-User-builder? make-UserList-User-builder
   UserList-User-user_id UserList-User-name
   has-UserList-User-name? UserList-User-last_seen
   has-UserList-User-last_seen? UserList-User-last_channel
   has-UserList-User-last_channel? has-UserList-User-extension?
   UserList-User-extension UserList-User-read
   UserList-User-write UserList-User? UserList-builder-users
   set-UserList-builder-users! clear-UserList-builder-users!
   clear-UserList-builder-extension!
   has-UserList-builder-extension?
   set-UserList-builder-extension! UserList-builder-extension
   UserList-builder-build UserList-builder?
   make-UserList-builder UserList-users has-UserList-extension?
   UserList-extension UserList-read UserList-write UserList?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (UserList-User-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-UserList-User-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-UserList-User-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-UserList-User-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (UserList-User-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-UserList-User-builder-user_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (UserList-User-builder-user_id-set! b0.0 0))
  (define (has-UserList-User-builder-user_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-UserList-User-builder-user_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (UserList-User-builder-user_id-set! b0.0 b1.1))
  (define (clear-UserList-User-builder-name! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (UserList-User-builder-name-set! b0.0 ""))
  (define (has-UserList-User-builder-name? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-UserList-User-builder-name! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (UserList-User-builder-name-set! b0.0 b1.1))
  (define (clear-UserList-User-builder-last_seen! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (UserList-User-builder-last_seen-set! b0.0 ""))
  (define (has-UserList-User-builder-last_seen? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-UserList-User-builder-last_seen! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (UserList-User-builder-last_seen-set! b0.0 b1.1))
  (define (clear-UserList-User-builder-last_channel! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (UserList-User-builder-last_channel-set! b0.0 0))
  (define (has-UserList-User-builder-last_channel? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-UserList-User-builder-last_channel! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (UserList-User-builder-last_channel-set! b0.0 b1.1))
  (define-record-type (UserList-User-builder
                        make-UserList-User-builder
                        UserList-User-builder?)
    (fields
      (mutable
        user_id
        UserList-User-builder-user_id
        UserList-User-builder-user_id-set!)
      (mutable
        name
        UserList-User-builder-name
        UserList-User-builder-name-set!)
      (mutable
        last_seen
        UserList-User-builder-last_seen
        UserList-User-builder-last_seen-set!)
      (mutable
        last_channel
        UserList-User-builder-last_channel
        UserList-User-builder-last_channel-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "user_id"
                protobuf:field-type-uint32 #f #t 0)
              (protobuf:make-field-descriptor 2 "name"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 3 "last_seen"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "last_channel"
                protobuf:field-type-uint32 #f #f 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor UserList-User)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (UserList-User-read r0.4)
    (protobuf:message-read (make-UserList-User-builder) r0.4))
  (define (UserList-User-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-UserList-User-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor UserList-User)
      e1.1))
  (define (UserList-User-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor UserList-User)
      e1.1))
  (define (has-UserList-User-name? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-UserList-User-last_seen? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-UserList-User-last_channel? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type UserList-User
    (fields
      (immutable user_id UserList-User-user_id)
      (immutable name UserList-User-name)
      (immutable last_seen UserList-User-last_seen)
      (immutable last_channel UserList-User-last_channel))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t))
  (define (UserList-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-UserList-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-UserList-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-UserList-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (UserList-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-UserList-builder-users! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (UserList-builder-users-set! b0.0 '()))
  (define (set-UserList-builder-users! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (UserList-builder-users-set! b0.0 b1.1))
  (define-record-type (UserList-builder
                        make-UserList-builder
                        UserList-builder?)
    (fields
      (mutable
        users
        UserList-builder-users
        UserList-builder-users-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "users"
                (protobuf:make-message-field-type-descriptor "User" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-UserList-User-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  UserList-User? #f #f)
                #t #f '())))
          (let ([b3.3 (b1.1 (record-type-descriptor UserList) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (UserList-read r0.4)
    (protobuf:message-read (make-UserList-builder) r0.4))
  (define (UserList-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-UserList-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor UserList)
      e1.1))
  (define (UserList-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor UserList)
      e1.1))
  (define-record-type UserList
    (fields (immutable users UserList-users))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto ContextAction)
  (export ContextAction-builder-session
   set-ContextAction-builder-session!
   has-ContextAction-builder-session?
   clear-ContextAction-builder-session!
   ContextAction-builder-channel_id
   set-ContextAction-builder-channel_id!
   has-ContextAction-builder-channel_id?
   clear-ContextAction-builder-channel_id!
   ContextAction-builder-action
   set-ContextAction-builder-action!
   has-ContextAction-builder-action?
   clear-ContextAction-builder-action!
   clear-ContextAction-builder-extension!
   has-ContextAction-builder-extension?
   set-ContextAction-builder-extension!
   ContextAction-builder-extension ContextAction-builder-build
   ContextAction-builder? make-ContextAction-builder
   ContextAction-session has-ContextAction-session?
   ContextAction-channel_id has-ContextAction-channel_id?
   ContextAction-action has-ContextAction-extension?
   ContextAction-extension ContextAction-read
   ContextAction-write ContextAction?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (ContextAction-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ContextAction-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ContextAction-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ContextAction-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ContextAction-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ContextAction-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ContextAction-builder-session-set! b0.0 0))
  (define (has-ContextAction-builder-session? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ContextAction-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ContextAction-builder-session-set! b0.0 b1.1))
  (define (clear-ContextAction-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ContextAction-builder-channel_id-set! b0.0 0))
  (define (has-ContextAction-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ContextAction-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ContextAction-builder-channel_id-set! b0.0 b1.1))
  (define (clear-ContextAction-builder-action! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ContextAction-builder-action-set! b0.0 ""))
  (define (has-ContextAction-builder-action? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-ContextAction-builder-action! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ContextAction-builder-action-set! b0.0 b1.1))
  (define-record-type (ContextAction-builder
                        make-ContextAction-builder
                        ContextAction-builder?)
    (fields
      (mutable
        session
        ContextAction-builder-session
        ContextAction-builder-session-set!)
      (mutable
        channel_id
        ContextAction-builder-channel_id
        ContextAction-builder-channel_id-set!)
      (mutable
        action
        ContextAction-builder-action
        ContextAction-builder-action-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "session"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "channel_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "action"
                protobuf:field-type-string #f #t "")))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ContextAction)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ContextAction-read r0.4)
    (protobuf:message-read (make-ContextAction-builder) r0.4))
  (define (ContextAction-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ContextAction-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ContextAction)
      e1.1))
  (define (ContextAction-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ContextAction)
      e1.1))
  (define (has-ContextAction-session? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-ContextAction-channel_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define-record-type ContextAction
    (fields
      (immutable session ContextAction-session)
      (immutable channel_id ContextAction-channel_id)
      (immutable action ContextAction-action))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto ContextActionModify)
  (export make-ContextActionModify-Context
   ContextActionModify-Context? ContextActionModify-Context
   make-ContextActionModify-Operation
   ContextActionModify-Operation? ContextActionModify-Operation
   ContextActionModify-builder-action
   set-ContextActionModify-builder-action!
   has-ContextActionModify-builder-action?
   clear-ContextActionModify-builder-action!
   ContextActionModify-builder-text
   set-ContextActionModify-builder-text!
   has-ContextActionModify-builder-text?
   clear-ContextActionModify-builder-text!
   ContextActionModify-builder-context
   set-ContextActionModify-builder-context!
   has-ContextActionModify-builder-context?
   clear-ContextActionModify-builder-context!
   ContextActionModify-builder-operation
   set-ContextActionModify-builder-operation!
   has-ContextActionModify-builder-operation?
   clear-ContextActionModify-builder-operation!
   clear-ContextActionModify-builder-extension!
   has-ContextActionModify-builder-extension?
   set-ContextActionModify-builder-extension!
   ContextActionModify-builder-extension
   ContextActionModify-builder-build
   ContextActionModify-builder?
   make-ContextActionModify-builder ContextActionModify-action
   ContextActionModify-text has-ContextActionModify-text?
   ContextActionModify-context has-ContextActionModify-context?
   ContextActionModify-operation
   has-ContextActionModify-operation?
   has-ContextActionModify-extension?
   ContextActionModify-extension ContextActionModify-read
   ContextActionModify-write ContextActionModify?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (ContextActionModify-Context? e0.0)
    (enum-set-member? e0.0 ContextActionModify-Context.1))
  (define ContextActionModify-Context.1
    (make-enumeration
      '(ContextActionModify-Context-User
         ContextActionModify-Context-Channel
         ContextActionModify-Context-Server)))
  (define-enumeration ContextActionModify-Context
    (ContextActionModify-Context-User
      ContextActionModify-Context-Channel
      ContextActionModify-Context-Server)
    make-ContextActionModify-Context)
  (define (ContextActionModify-Operation? e0.0)
    (enum-set-member? e0.0 ContextActionModify-Operation.1))
  (define ContextActionModify-Operation.1
    (make-enumeration
      '(ContextActionModify-Operation-Remove
         ContextActionModify-Operation-Add)))
  (define-enumeration ContextActionModify-Operation
    (ContextActionModify-Operation-Remove
      ContextActionModify-Operation-Add)
    make-ContextActionModify-Operation)
  (define (ContextActionModify-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ContextActionModify-builder-extension! b0.0
           b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ContextActionModify-builder-extension? b0.0
           b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ContextActionModify-builder-extension! b0.0
           b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ContextActionModify-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ContextActionModify-builder-action! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ContextActionModify-builder-action-set! b0.0 ""))
  (define (has-ContextActionModify-builder-action? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ContextActionModify-builder-action! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ContextActionModify-builder-action-set! b0.0 b1.1))
  (define (clear-ContextActionModify-builder-text! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ContextActionModify-builder-text-set! b0.0 ""))
  (define (has-ContextActionModify-builder-text? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ContextActionModify-builder-text! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ContextActionModify-builder-text-set! b0.0 b1.1))
  (define (clear-ContextActionModify-builder-context! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ContextActionModify-builder-context-set! b0.0 0))
  (define (has-ContextActionModify-builder-context? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-ContextActionModify-builder-context! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ContextActionModify-builder-context-set! b0.0 b1.1))
  (define (clear-ContextActionModify-builder-operation! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (ContextActionModify-builder-operation-set!
      b0.0
      (ContextActionModify-Operation
        ContextActionModify-Operation-Remove)))
  (define (has-ContextActionModify-builder-operation? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-ContextActionModify-builder-operation! b0.0
           b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (ContextActionModify-builder-operation-set! b0.0 b1.1))
  (define-record-type (ContextActionModify-builder
                        make-ContextActionModify-builder
                        ContextActionModify-builder?)
    (fields
      (mutable
        action
        ContextActionModify-builder-action
        ContextActionModify-builder-action-set!)
      (mutable
        text
        ContextActionModify-builder-text
        ContextActionModify-builder-text-set!)
      (mutable
        context
        ContextActionModify-builder-context
        ContextActionModify-builder-context-set!)
      (mutable
        operation
        ContextActionModify-builder-operation
        ContextActionModify-builder-operation-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "action"
                protobuf:field-type-string #f #t "")
              (protobuf:make-field-descriptor 2 "text"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 3 "context"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 4 "operation"
                (protobuf:make-enum-field-type-descriptor "Operation" 'varint
                  (lambda (p0.0 p1.0)
                    (protobuf:write-varint
                      p0.0
                      (case p1.0
                        [(ContextActionModify-Operation-Remove) 1]
                        [(ContextActionModify-Operation-Add) 0])))
                  (lambda (p0.0)
                    (case (protobuf:read-varint p0.0)
                      [(1)
                       (ContextActionModify-Operation
                         ContextActionModify-Operation-Remove)]
                      [(0)
                       (ContextActionModify-Operation
                         ContextActionModify-Operation-Add)]))
                  ContextActionModify-Operation? #f #f)
                #f #f
                (ContextActionModify-Operation
                  ContextActionModify-Operation-Remove))))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ContextActionModify)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ContextActionModify-read r0.4)
    (protobuf:message-read
      (make-ContextActionModify-builder)
      r0.4))
  (define (ContextActionModify-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ContextActionModify-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ContextActionModify)
      e1.1))
  (define (ContextActionModify-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ContextActionModify)
      e1.1))
  (define (has-ContextActionModify-text? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-ContextActionModify-context? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-ContextActionModify-operation? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type ContextActionModify
    (fields
      (immutable action ContextActionModify-action)
      (immutable text ContextActionModify-text)
      (immutable context ContextActionModify-context)
      (immutable operation ContextActionModify-operation))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto CryptSetup)
  (export CryptSetup-builder-key set-CryptSetup-builder-key!
   has-CryptSetup-builder-key? clear-CryptSetup-builder-key!
   CryptSetup-builder-client_nonce
   set-CryptSetup-builder-client_nonce!
   has-CryptSetup-builder-client_nonce?
   clear-CryptSetup-builder-client_nonce!
   CryptSetup-builder-server_nonce
   set-CryptSetup-builder-server_nonce!
   has-CryptSetup-builder-server_nonce?
   clear-CryptSetup-builder-server_nonce!
   clear-CryptSetup-builder-extension!
   has-CryptSetup-builder-extension?
   set-CryptSetup-builder-extension!
   CryptSetup-builder-extension CryptSetup-builder-build
   CryptSetup-builder? make-CryptSetup-builder CryptSetup-key
   has-CryptSetup-key? CryptSetup-client_nonce
   has-CryptSetup-client_nonce? CryptSetup-server_nonce
   has-CryptSetup-server_nonce? has-CryptSetup-extension?
   CryptSetup-extension CryptSetup-read CryptSetup-write
   CryptSetup?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (CryptSetup-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-CryptSetup-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-CryptSetup-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-CryptSetup-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (CryptSetup-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-CryptSetup-builder-key! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (CryptSetup-builder-key-set! b0.0 #vu8()))
  (define (has-CryptSetup-builder-key? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-CryptSetup-builder-key! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (CryptSetup-builder-key-set! b0.0 b1.1))
  (define (clear-CryptSetup-builder-client_nonce! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (CryptSetup-builder-client_nonce-set! b0.0 #vu8()))
  (define (has-CryptSetup-builder-client_nonce? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-CryptSetup-builder-client_nonce! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (CryptSetup-builder-client_nonce-set! b0.0 b1.1))
  (define (clear-CryptSetup-builder-server_nonce! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (CryptSetup-builder-server_nonce-set! b0.0 #vu8()))
  (define (has-CryptSetup-builder-server_nonce? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-CryptSetup-builder-server_nonce! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (CryptSetup-builder-server_nonce-set! b0.0 b1.1))
  (define-record-type (CryptSetup-builder
                        make-CryptSetup-builder
                        CryptSetup-builder?)
    (fields
      (mutable
        key
        CryptSetup-builder-key
        CryptSetup-builder-key-set!)
      (mutable
        client_nonce
        CryptSetup-builder-client_nonce
        CryptSetup-builder-client_nonce-set!)
      (mutable
        server_nonce
        CryptSetup-builder-server_nonce
        CryptSetup-builder-server_nonce-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "key"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 2 "client_nonce"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 3 "server_nonce"
                protobuf:field-type-bytes #f #f #vu8())))
          (let ([b3.3 (b1.1
                        (record-type-descriptor CryptSetup)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (CryptSetup-read r0.4)
    (protobuf:message-read (make-CryptSetup-builder) r0.4))
  (define (CryptSetup-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-CryptSetup-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor CryptSetup)
      e1.1))
  (define (CryptSetup-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor CryptSetup)
      e1.1))
  (define (has-CryptSetup-key? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-CryptSetup-client_nonce? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-CryptSetup-server_nonce? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define-record-type CryptSetup
    (fields
      (immutable key CryptSetup-key)
      (immutable client_nonce CryptSetup-client_nonce)
      (immutable server_nonce CryptSetup-server_nonce))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto QueryUsers)
  (export QueryUsers-builder-ids set-QueryUsers-builder-ids!
   clear-QueryUsers-builder-ids! QueryUsers-builder-names
   set-QueryUsers-builder-names!
   clear-QueryUsers-builder-names!
   clear-QueryUsers-builder-extension!
   has-QueryUsers-builder-extension?
   set-QueryUsers-builder-extension!
   QueryUsers-builder-extension QueryUsers-builder-build
   QueryUsers-builder? make-QueryUsers-builder QueryUsers-ids
   QueryUsers-names has-QueryUsers-extension?
   QueryUsers-extension QueryUsers-read QueryUsers-write
   QueryUsers?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (QueryUsers-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-QueryUsers-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-QueryUsers-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-QueryUsers-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (QueryUsers-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-QueryUsers-builder-ids! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (QueryUsers-builder-ids-set! b0.0 '()))
  (define (set-QueryUsers-builder-ids! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (QueryUsers-builder-ids-set! b0.0 b1.1))
  (define (clear-QueryUsers-builder-names! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (QueryUsers-builder-names-set! b0.0 '()))
  (define (set-QueryUsers-builder-names! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (QueryUsers-builder-names-set! b0.0 b1.1))
  (define-record-type (QueryUsers-builder
                        make-QueryUsers-builder
                        QueryUsers-builder?)
    (fields
      (mutable
        ids
        QueryUsers-builder-ids
        QueryUsers-builder-ids-set!)
      (mutable
        names
        QueryUsers-builder-names
        QueryUsers-builder-names-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "ids"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 2 "names"
                protobuf:field-type-string #t #f '())))
          (let ([b3.3 (b1.1
                        (record-type-descriptor QueryUsers)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (QueryUsers-read r0.4)
    (protobuf:message-read (make-QueryUsers-builder) r0.4))
  (define (QueryUsers-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-QueryUsers-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor QueryUsers)
      e1.1))
  (define (QueryUsers-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor QueryUsers)
      e1.1))
  (define-record-type QueryUsers
    (fields
      (immutable ids QueryUsers-ids)
      (immutable names QueryUsers-names))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto ACL)
  (export ACL-ChanGroup-builder-name
   set-ACL-ChanGroup-builder-name!
   has-ACL-ChanGroup-builder-name?
   clear-ACL-ChanGroup-builder-name!
   ACL-ChanGroup-builder-inherited
   set-ACL-ChanGroup-builder-inherited!
   has-ACL-ChanGroup-builder-inherited?
   clear-ACL-ChanGroup-builder-inherited!
   ACL-ChanGroup-builder-inherit
   set-ACL-ChanGroup-builder-inherit!
   has-ACL-ChanGroup-builder-inherit?
   clear-ACL-ChanGroup-builder-inherit!
   ACL-ChanGroup-builder-inheritable
   set-ACL-ChanGroup-builder-inheritable!
   has-ACL-ChanGroup-builder-inheritable?
   clear-ACL-ChanGroup-builder-inheritable!
   ACL-ChanGroup-builder-add set-ACL-ChanGroup-builder-add!
   clear-ACL-ChanGroup-builder-add!
   ACL-ChanGroup-builder-remove
   set-ACL-ChanGroup-builder-remove!
   clear-ACL-ChanGroup-builder-remove!
   ACL-ChanGroup-builder-inherited_members
   set-ACL-ChanGroup-builder-inherited_members!
   clear-ACL-ChanGroup-builder-inherited_members!
   clear-ACL-ChanGroup-builder-extension!
   has-ACL-ChanGroup-builder-extension?
   set-ACL-ChanGroup-builder-extension!
   ACL-ChanGroup-builder-extension ACL-ChanGroup-builder-build
   ACL-ChanGroup-builder? make-ACL-ChanGroup-builder
   ACL-ChanGroup-name ACL-ChanGroup-inherited
   has-ACL-ChanGroup-inherited? ACL-ChanGroup-inherit
   has-ACL-ChanGroup-inherit? ACL-ChanGroup-inheritable
   has-ACL-ChanGroup-inheritable? ACL-ChanGroup-add
   ACL-ChanGroup-remove ACL-ChanGroup-inherited_members
   has-ACL-ChanGroup-extension? ACL-ChanGroup-extension
   ACL-ChanGroup-read ACL-ChanGroup-write ACL-ChanGroup?
   ACL-ChanACL-builder-apply_here
   set-ACL-ChanACL-builder-apply_here!
   has-ACL-ChanACL-builder-apply_here?
   clear-ACL-ChanACL-builder-apply_here!
   ACL-ChanACL-builder-apply_subs
   set-ACL-ChanACL-builder-apply_subs!
   has-ACL-ChanACL-builder-apply_subs?
   clear-ACL-ChanACL-builder-apply_subs!
   ACL-ChanACL-builder-inherited
   set-ACL-ChanACL-builder-inherited!
   has-ACL-ChanACL-builder-inherited?
   clear-ACL-ChanACL-builder-inherited!
   ACL-ChanACL-builder-user_id set-ACL-ChanACL-builder-user_id!
   has-ACL-ChanACL-builder-user_id?
   clear-ACL-ChanACL-builder-user_id! ACL-ChanACL-builder-group
   set-ACL-ChanACL-builder-group!
   has-ACL-ChanACL-builder-group?
   clear-ACL-ChanACL-builder-group! ACL-ChanACL-builder-grant
   set-ACL-ChanACL-builder-grant!
   has-ACL-ChanACL-builder-grant?
   clear-ACL-ChanACL-builder-grant! ACL-ChanACL-builder-deny
   set-ACL-ChanACL-builder-deny! has-ACL-ChanACL-builder-deny?
   clear-ACL-ChanACL-builder-deny!
   clear-ACL-ChanACL-builder-extension!
   has-ACL-ChanACL-builder-extension?
   set-ACL-ChanACL-builder-extension!
   ACL-ChanACL-builder-extension ACL-ChanACL-builder-build
   ACL-ChanACL-builder? make-ACL-ChanACL-builder
   ACL-ChanACL-apply_here has-ACL-ChanACL-apply_here?
   ACL-ChanACL-apply_subs has-ACL-ChanACL-apply_subs?
   ACL-ChanACL-inherited has-ACL-ChanACL-inherited?
   ACL-ChanACL-user_id has-ACL-ChanACL-user_id?
   ACL-ChanACL-group has-ACL-ChanACL-group? ACL-ChanACL-grant
   has-ACL-ChanACL-grant? ACL-ChanACL-deny
   has-ACL-ChanACL-deny? has-ACL-ChanACL-extension?
   ACL-ChanACL-extension ACL-ChanACL-read ACL-ChanACL-write
   ACL-ChanACL? ACL-builder-channel_id
   set-ACL-builder-channel_id! has-ACL-builder-channel_id?
   clear-ACL-builder-channel_id! ACL-builder-inherit_acls
   set-ACL-builder-inherit_acls! has-ACL-builder-inherit_acls?
   clear-ACL-builder-inherit_acls! ACL-builder-groups
   set-ACL-builder-groups! clear-ACL-builder-groups!
   ACL-builder-acls set-ACL-builder-acls!
   clear-ACL-builder-acls! ACL-builder-query
   set-ACL-builder-query! has-ACL-builder-query?
   clear-ACL-builder-query! clear-ACL-builder-extension!
   has-ACL-builder-extension? set-ACL-builder-extension!
   ACL-builder-extension ACL-builder-build ACL-builder?
   make-ACL-builder ACL-channel_id ACL-inherit_acls
   has-ACL-inherit_acls? ACL-groups ACL-acls ACL-query
   has-ACL-query? has-ACL-extension? ACL-extension ACL-read
   ACL-write ACL?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (ACL-ChanGroup-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ACL-ChanGroup-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ACL-ChanGroup-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ACL-ChanGroup-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ACL-ChanGroup-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ACL-ChanGroup-builder-name! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ACL-ChanGroup-builder-name-set! b0.0 ""))
  (define (has-ACL-ChanGroup-builder-name? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ACL-ChanGroup-builder-name! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ACL-ChanGroup-builder-name-set! b0.0 b1.1))
  (define (clear-ACL-ChanGroup-builder-inherited! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ACL-ChanGroup-builder-inherited-set! b0.0 #t))
  (define (has-ACL-ChanGroup-builder-inherited? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ACL-ChanGroup-builder-inherited! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ACL-ChanGroup-builder-inherited-set! b0.0 b1.1))
  (define (clear-ACL-ChanGroup-builder-inherit! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ACL-ChanGroup-builder-inherit-set! b0.0 #t))
  (define (has-ACL-ChanGroup-builder-inherit? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-ACL-ChanGroup-builder-inherit! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ACL-ChanGroup-builder-inherit-set! b0.0 b1.1))
  (define (clear-ACL-ChanGroup-builder-inheritable! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (ACL-ChanGroup-builder-inheritable-set! b0.0 #t))
  (define (has-ACL-ChanGroup-builder-inheritable? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-ACL-ChanGroup-builder-inheritable! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (ACL-ChanGroup-builder-inheritable-set! b0.0 b1.1))
  (define (clear-ACL-ChanGroup-builder-add! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (ACL-ChanGroup-builder-add-set! b0.0 '()))
  (define (set-ACL-ChanGroup-builder-add! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (ACL-ChanGroup-builder-add-set! b0.0 b1.1))
  (define (clear-ACL-ChanGroup-builder-remove! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (ACL-ChanGroup-builder-remove-set! b0.0 '()))
  (define (set-ACL-ChanGroup-builder-remove! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (ACL-ChanGroup-builder-remove-set! b0.0 b1.1))
  (define (clear-ACL-ChanGroup-builder-inherited_members!
           b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 7))
    (ACL-ChanGroup-builder-inherited_members-set! b0.0 '()))
  (define (set-ACL-ChanGroup-builder-inherited_members! b0.0
           b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 7)
      b1.1)
    (ACL-ChanGroup-builder-inherited_members-set! b0.0 b1.1))
  (define-record-type (ACL-ChanGroup-builder
                        make-ACL-ChanGroup-builder
                        ACL-ChanGroup-builder?)
    (fields
      (mutable
        name
        ACL-ChanGroup-builder-name
        ACL-ChanGroup-builder-name-set!)
      (mutable
        inherited
        ACL-ChanGroup-builder-inherited
        ACL-ChanGroup-builder-inherited-set!)
      (mutable
        inherit
        ACL-ChanGroup-builder-inherit
        ACL-ChanGroup-builder-inherit-set!)
      (mutable
        inheritable
        ACL-ChanGroup-builder-inheritable
        ACL-ChanGroup-builder-inheritable-set!)
      (mutable
        add
        ACL-ChanGroup-builder-add
        ACL-ChanGroup-builder-add-set!)
      (mutable
        remove
        ACL-ChanGroup-builder-remove
        ACL-ChanGroup-builder-remove-set!)
      (mutable
        inherited_members
        ACL-ChanGroup-builder-inherited_members
        ACL-ChanGroup-builder-inherited_members-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "name"
                protobuf:field-type-string #f #t "")
              (protobuf:make-field-descriptor 2 "inherited"
                protobuf:field-type-bool #f #f #t)
              (protobuf:make-field-descriptor 3 "inherit"
                protobuf:field-type-bool #f #f #t)
              (protobuf:make-field-descriptor 4 "inheritable"
                protobuf:field-type-bool #f #f #t)
              (protobuf:make-field-descriptor 5 "add"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 6 "remove"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 7 "inherited_members"
                protobuf:field-type-uint32 #t #f '())))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ACL-ChanGroup)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ACL-ChanGroup-read r0.4)
    (protobuf:message-read (make-ACL-ChanGroup-builder) r0.4))
  (define (ACL-ChanGroup-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ACL-ChanGroup-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ACL-ChanGroup)
      e1.1))
  (define (ACL-ChanGroup-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ACL-ChanGroup)
      e1.1))
  (define (has-ACL-ChanGroup-inherited? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-ACL-ChanGroup-inherit? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-ACL-ChanGroup-inheritable? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type ACL-ChanGroup
    (fields (immutable name ACL-ChanGroup-name)
      (immutable inherited ACL-ChanGroup-inherited)
      (immutable inherit ACL-ChanGroup-inherit)
      (immutable inheritable ACL-ChanGroup-inheritable)
      (immutable add ACL-ChanGroup-add)
      (immutable remove ACL-ChanGroup-remove)
      (immutable
        inherited_members
        ACL-ChanGroup-inherited_members))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t))
  (define (ACL-ChanACL-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ACL-ChanACL-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ACL-ChanACL-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ACL-ChanACL-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ACL-ChanACL-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ACL-ChanACL-builder-apply_here! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ACL-ChanACL-builder-apply_here-set! b0.0 #t))
  (define (has-ACL-ChanACL-builder-apply_here? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ACL-ChanACL-builder-apply_here! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ACL-ChanACL-builder-apply_here-set! b0.0 b1.1))
  (define (clear-ACL-ChanACL-builder-apply_subs! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ACL-ChanACL-builder-apply_subs-set! b0.0 #t))
  (define (has-ACL-ChanACL-builder-apply_subs? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ACL-ChanACL-builder-apply_subs! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ACL-ChanACL-builder-apply_subs-set! b0.0 b1.1))
  (define (clear-ACL-ChanACL-builder-inherited! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ACL-ChanACL-builder-inherited-set! b0.0 #t))
  (define (has-ACL-ChanACL-builder-inherited? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-ACL-ChanACL-builder-inherited! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ACL-ChanACL-builder-inherited-set! b0.0 b1.1))
  (define (clear-ACL-ChanACL-builder-user_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (ACL-ChanACL-builder-user_id-set! b0.0 0))
  (define (has-ACL-ChanACL-builder-user_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-ACL-ChanACL-builder-user_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (ACL-ChanACL-builder-user_id-set! b0.0 b1.1))
  (define (clear-ACL-ChanACL-builder-group! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (ACL-ChanACL-builder-group-set! b0.0 ""))
  (define (has-ACL-ChanACL-builder-group? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-ACL-ChanACL-builder-group! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (ACL-ChanACL-builder-group-set! b0.0 b1.1))
  (define (clear-ACL-ChanACL-builder-grant! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (ACL-ChanACL-builder-grant-set! b0.0 0))
  (define (has-ACL-ChanACL-builder-grant? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 6)))
  (define (set-ACL-ChanACL-builder-grant! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (ACL-ChanACL-builder-grant-set! b0.0 b1.1))
  (define (clear-ACL-ChanACL-builder-deny! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 7))
    (ACL-ChanACL-builder-deny-set! b0.0 0))
  (define (has-ACL-ChanACL-builder-deny? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 7)))
  (define (set-ACL-ChanACL-builder-deny! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 7)
      b1.1)
    (ACL-ChanACL-builder-deny-set! b0.0 b1.1))
  (define-record-type (ACL-ChanACL-builder
                        make-ACL-ChanACL-builder
                        ACL-ChanACL-builder?)
    (fields
      (mutable
        apply_here
        ACL-ChanACL-builder-apply_here
        ACL-ChanACL-builder-apply_here-set!)
      (mutable
        apply_subs
        ACL-ChanACL-builder-apply_subs
        ACL-ChanACL-builder-apply_subs-set!)
      (mutable
        inherited
        ACL-ChanACL-builder-inherited
        ACL-ChanACL-builder-inherited-set!)
      (mutable
        user_id
        ACL-ChanACL-builder-user_id
        ACL-ChanACL-builder-user_id-set!)
      (mutable
        group
        ACL-ChanACL-builder-group
        ACL-ChanACL-builder-group-set!)
      (mutable
        grant
        ACL-ChanACL-builder-grant
        ACL-ChanACL-builder-grant-set!)
      (mutable
        deny
        ACL-ChanACL-builder-deny
        ACL-ChanACL-builder-deny-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "apply_here"
                protobuf:field-type-bool #f #f #t)
              (protobuf:make-field-descriptor 2 "apply_subs"
                protobuf:field-type-bool #f #f #t)
              (protobuf:make-field-descriptor 3 "inherited"
                protobuf:field-type-bool #f #f #t)
              (protobuf:make-field-descriptor 4 "user_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 5 "group"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 6 "grant"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 7 "deny"
                protobuf:field-type-uint32 #f #f 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ACL-ChanACL)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ACL-ChanACL-read r0.4)
    (protobuf:message-read (make-ACL-ChanACL-builder) r0.4))
  (define (ACL-ChanACL-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ACL-ChanACL-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ACL-ChanACL)
      e1.1))
  (define (ACL-ChanACL-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ACL-ChanACL)
      e1.1))
  (define (has-ACL-ChanACL-apply_here? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-ACL-ChanACL-apply_subs? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-ACL-ChanACL-inherited? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-ACL-ChanACL-user_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-ACL-ChanACL-group? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-ACL-ChanACL-grant? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 6)))
  (define (has-ACL-ChanACL-deny? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 7)))
  (define-record-type ACL-ChanACL
    (fields (immutable apply_here ACL-ChanACL-apply_here)
      (immutable apply_subs ACL-ChanACL-apply_subs)
      (immutable inherited ACL-ChanACL-inherited)
      (immutable user_id ACL-ChanACL-user_id)
      (immutable group ACL-ChanACL-group)
      (immutable grant ACL-ChanACL-grant)
      (immutable deny ACL-ChanACL-deny))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t))
  (define (ACL-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ACL-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ACL-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ACL-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ACL-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ACL-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ACL-builder-channel_id-set! b0.0 0))
  (define (has-ACL-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ACL-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ACL-builder-channel_id-set! b0.0 b1.1))
  (define (clear-ACL-builder-inherit_acls! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ACL-builder-inherit_acls-set! b0.0 #t))
  (define (has-ACL-builder-inherit_acls? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ACL-builder-inherit_acls! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ACL-builder-inherit_acls-set! b0.0 b1.1))
  (define (clear-ACL-builder-groups! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ACL-builder-groups-set! b0.0 '()))
  (define (set-ACL-builder-groups! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ACL-builder-groups-set! b0.0 b1.1))
  (define (clear-ACL-builder-acls! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (ACL-builder-acls-set! b0.0 '()))
  (define (set-ACL-builder-acls! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (ACL-builder-acls-set! b0.0 b1.1))
  (define (clear-ACL-builder-query! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (ACL-builder-query-set! b0.0 #f))
  (define (has-ACL-builder-query? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-ACL-builder-query! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (ACL-builder-query-set! b0.0 b1.1))
  (define-record-type (ACL-builder
                        make-ACL-builder
                        ACL-builder?)
    (fields
      (mutable
        channel_id
        ACL-builder-channel_id
        ACL-builder-channel_id-set!)
      (mutable
        inherit_acls
        ACL-builder-inherit_acls
        ACL-builder-inherit_acls-set!)
      (mutable groups ACL-builder-groups ACL-builder-groups-set!)
      (mutable acls ACL-builder-acls ACL-builder-acls-set!)
      (mutable query ACL-builder-query ACL-builder-query-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "channel_id"
                protobuf:field-type-uint32 #f #t 0)
              (protobuf:make-field-descriptor 2 "inherit_acls"
                protobuf:field-type-bool #f #f #t)
              (protobuf:make-field-descriptor 3 "groups"
                (protobuf:make-message-field-type-descriptor "ChanGroup" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-ACL-ChanGroup-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  ACL-ChanGroup? #f #f)
                #t #f '())
              (protobuf:make-field-descriptor 4 "acls"
                (protobuf:make-message-field-type-descriptor "ChanACL" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-ACL-ChanACL-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  ACL-ChanACL? #f #f)
                #t #f '())
              (protobuf:make-field-descriptor 5 "query"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1 (record-type-descriptor ACL) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ACL-read r0.4)
    (protobuf:message-read (make-ACL-builder) r0.4))
  (define (ACL-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ACL-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ACL)
      e1.1))
  (define (ACL-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ACL)
      e1.1))
  (define (has-ACL-inherit_acls? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-ACL-query? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define-record-type ACL
    (fields (immutable channel_id ACL-channel_id)
      (immutable inherit_acls ACL-inherit_acls)
      (immutable groups ACL-groups) (immutable acls ACL-acls)
      (immutable query ACL-query))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto PermissionDenied)
  (export make-PermissionDenied-DenyType
   PermissionDenied-DenyType? PermissionDenied-DenyType
   PermissionDenied-builder-permission
   set-PermissionDenied-builder-permission!
   has-PermissionDenied-builder-permission?
   clear-PermissionDenied-builder-permission!
   PermissionDenied-builder-channel_id
   set-PermissionDenied-builder-channel_id!
   has-PermissionDenied-builder-channel_id?
   clear-PermissionDenied-builder-channel_id!
   PermissionDenied-builder-session
   set-PermissionDenied-builder-session!
   has-PermissionDenied-builder-session?
   clear-PermissionDenied-builder-session!
   PermissionDenied-builder-reason
   set-PermissionDenied-builder-reason!
   has-PermissionDenied-builder-reason?
   clear-PermissionDenied-builder-reason!
   PermissionDenied-builder-type
   set-PermissionDenied-builder-type!
   has-PermissionDenied-builder-type?
   clear-PermissionDenied-builder-type!
   PermissionDenied-builder-name
   set-PermissionDenied-builder-name!
   has-PermissionDenied-builder-name?
   clear-PermissionDenied-builder-name!
   clear-PermissionDenied-builder-extension!
   has-PermissionDenied-builder-extension?
   set-PermissionDenied-builder-extension!
   PermissionDenied-builder-extension
   PermissionDenied-builder-build PermissionDenied-builder?
   make-PermissionDenied-builder PermissionDenied-permission
   has-PermissionDenied-permission? PermissionDenied-channel_id
   has-PermissionDenied-channel_id? PermissionDenied-session
   has-PermissionDenied-session? PermissionDenied-reason
   has-PermissionDenied-reason? PermissionDenied-type
   has-PermissionDenied-type? PermissionDenied-name
   has-PermissionDenied-name? has-PermissionDenied-extension?
   PermissionDenied-extension PermissionDenied-read
   PermissionDenied-write PermissionDenied?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (PermissionDenied-DenyType? e0.0)
    (enum-set-member? e0.0 PermissionDenied-DenyType.1))
  (define PermissionDenied-DenyType.1
    (make-enumeration
      '(PermissionDenied-DenyType-ChannelCountLimit PermissionDenied-DenyType-NestingLimit
         PermissionDenied-DenyType-ChannelFull
         PermissionDenied-DenyType-UserName
         PermissionDenied-DenyType-MissingCertificate
         PermissionDenied-DenyType-TemporaryChannel
         PermissionDenied-DenyType-H9K
         PermissionDenied-DenyType-TextTooLong
         PermissionDenied-DenyType-ChannelName
         PermissionDenied-DenyType-SuperUser
         PermissionDenied-DenyType-Permission
         PermissionDenied-DenyType-Text)))
  (define-enumeration PermissionDenied-DenyType
    (PermissionDenied-DenyType-ChannelCountLimit PermissionDenied-DenyType-NestingLimit
      PermissionDenied-DenyType-ChannelFull
      PermissionDenied-DenyType-UserName
      PermissionDenied-DenyType-MissingCertificate
      PermissionDenied-DenyType-TemporaryChannel
      PermissionDenied-DenyType-H9K
      PermissionDenied-DenyType-TextTooLong
      PermissionDenied-DenyType-ChannelName
      PermissionDenied-DenyType-SuperUser
      PermissionDenied-DenyType-Permission
      PermissionDenied-DenyType-Text)
    make-PermissionDenied-DenyType)
  (define (PermissionDenied-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-PermissionDenied-builder-extension! b0.0
           b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-PermissionDenied-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-PermissionDenied-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (PermissionDenied-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-PermissionDenied-builder-permission! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (PermissionDenied-builder-permission-set! b0.0 0))
  (define (has-PermissionDenied-builder-permission? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-PermissionDenied-builder-permission! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (PermissionDenied-builder-permission-set! b0.0 b1.1))
  (define (clear-PermissionDenied-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (PermissionDenied-builder-channel_id-set! b0.0 0))
  (define (has-PermissionDenied-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-PermissionDenied-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (PermissionDenied-builder-channel_id-set! b0.0 b1.1))
  (define (clear-PermissionDenied-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (PermissionDenied-builder-session-set! b0.0 0))
  (define (has-PermissionDenied-builder-session? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-PermissionDenied-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (PermissionDenied-builder-session-set! b0.0 b1.1))
  (define (clear-PermissionDenied-builder-reason! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (PermissionDenied-builder-reason-set! b0.0 ""))
  (define (has-PermissionDenied-builder-reason? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-PermissionDenied-builder-reason! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (PermissionDenied-builder-reason-set! b0.0 b1.1))
  (define (clear-PermissionDenied-builder-type! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (PermissionDenied-builder-type-set!
      b0.0
      (PermissionDenied-DenyType
        PermissionDenied-DenyType-ChannelCountLimit)))
  (define (has-PermissionDenied-builder-type? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-PermissionDenied-builder-type! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (PermissionDenied-builder-type-set! b0.0 b1.1))
  (define (clear-PermissionDenied-builder-name! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (PermissionDenied-builder-name-set! b0.0 ""))
  (define (has-PermissionDenied-builder-name? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 6)))
  (define (set-PermissionDenied-builder-name! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (PermissionDenied-builder-name-set! b0.0 b1.1))
  (define-record-type (PermissionDenied-builder
                        make-PermissionDenied-builder
                        PermissionDenied-builder?)
    (fields
      (mutable
        permission
        PermissionDenied-builder-permission
        PermissionDenied-builder-permission-set!)
      (mutable
        channel_id
        PermissionDenied-builder-channel_id
        PermissionDenied-builder-channel_id-set!)
      (mutable
        session
        PermissionDenied-builder-session
        PermissionDenied-builder-session-set!)
      (mutable
        reason
        PermissionDenied-builder-reason
        PermissionDenied-builder-reason-set!)
      (mutable
        type
        PermissionDenied-builder-type
        PermissionDenied-builder-type-set!)
      (mutable
        name
        PermissionDenied-builder-name
        PermissionDenied-builder-name-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "permission"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "channel_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "session"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 4 "reason"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 5 "type"
                (protobuf:make-enum-field-type-descriptor "DenyType" 'varint
                  (lambda (p0.0 p1.0)
                    (protobuf:write-varint
                      p0.0
                      (case p1.0
                        [(PermissionDenied-DenyType-ChannelCountLimit) 11]
                        [(PermissionDenied-DenyType-NestingLimit) 10]
                        [(PermissionDenied-DenyType-ChannelFull) 9]
                        [(PermissionDenied-DenyType-UserName) 8]
                        [(PermissionDenied-DenyType-MissingCertificate) 7]
                        [(PermissionDenied-DenyType-TemporaryChannel) 6]
                        [(PermissionDenied-DenyType-H9K) 5]
                        [(PermissionDenied-DenyType-TextTooLong) 4]
                        [(PermissionDenied-DenyType-ChannelName) 3]
                        [(PermissionDenied-DenyType-SuperUser) 2]
                        [(PermissionDenied-DenyType-Permission) 1]
                        [(PermissionDenied-DenyType-Text) 0])))
                  (lambda (p0.0)
                    (case (protobuf:read-varint p0.0)
                      [(11)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-ChannelCountLimit)]
                      [(10)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-NestingLimit)]
                      [(9)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-ChannelFull)]
                      [(8)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-UserName)]
                      [(7)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-MissingCertificate)]
                      [(6)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-TemporaryChannel)]
                      [(5)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-H9K)]
                      [(4)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-TextTooLong)]
                      [(3)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-ChannelName)]
                      [(2)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-SuperUser)]
                      [(1)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-Permission)]
                      [(0)
                       (PermissionDenied-DenyType
                         PermissionDenied-DenyType-Text)]))
                  PermissionDenied-DenyType? #f #f)
                #f #f
                (PermissionDenied-DenyType
                  PermissionDenied-DenyType-ChannelCountLimit))
              (protobuf:make-field-descriptor 6 "name"
                protobuf:field-type-string #f #f "")))
          (let ([b3.3 (b1.1
                        (record-type-descriptor PermissionDenied)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (PermissionDenied-read r0.4)
    (protobuf:message-read
      (make-PermissionDenied-builder)
      r0.4))
  (define (PermissionDenied-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-PermissionDenied-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor PermissionDenied)
      e1.1))
  (define (PermissionDenied-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor PermissionDenied)
      e1.1))
  (define (has-PermissionDenied-permission? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-PermissionDenied-channel_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-PermissionDenied-session? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-PermissionDenied-reason? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-PermissionDenied-type? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-PermissionDenied-name? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 6)))
  (define-record-type PermissionDenied
    (fields (immutable permission PermissionDenied-permission)
      (immutable channel_id PermissionDenied-channel_id)
      (immutable session PermissionDenied-session)
      (immutable reason PermissionDenied-reason)
      (immutable type PermissionDenied-type)
      (immutable name PermissionDenied-name))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto TextMessage)
  (export TextMessage-builder-actor
   set-TextMessage-builder-actor!
   has-TextMessage-builder-actor?
   clear-TextMessage-builder-actor! TextMessage-builder-session
   set-TextMessage-builder-session!
   clear-TextMessage-builder-session!
   TextMessage-builder-channel_id
   set-TextMessage-builder-channel_id!
   clear-TextMessage-builder-channel_id!
   TextMessage-builder-tree_id set-TextMessage-builder-tree_id!
   clear-TextMessage-builder-tree_id!
   TextMessage-builder-message set-TextMessage-builder-message!
   has-TextMessage-builder-message?
   clear-TextMessage-builder-message!
   clear-TextMessage-builder-extension!
   has-TextMessage-builder-extension?
   set-TextMessage-builder-extension!
   TextMessage-builder-extension TextMessage-builder-build
   TextMessage-builder? make-TextMessage-builder
   TextMessage-actor has-TextMessage-actor? TextMessage-session
   TextMessage-channel_id TextMessage-tree_id
   TextMessage-message has-TextMessage-extension?
   TextMessage-extension TextMessage-read TextMessage-write
   TextMessage?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (TextMessage-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-TextMessage-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-TextMessage-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-TextMessage-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (TextMessage-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-TextMessage-builder-actor! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (TextMessage-builder-actor-set! b0.0 0))
  (define (has-TextMessage-builder-actor? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-TextMessage-builder-actor! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (TextMessage-builder-actor-set! b0.0 b1.1))
  (define (clear-TextMessage-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (TextMessage-builder-session-set! b0.0 '()))
  (define (set-TextMessage-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (TextMessage-builder-session-set! b0.0 b1.1))
  (define (clear-TextMessage-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (TextMessage-builder-channel_id-set! b0.0 '()))
  (define (set-TextMessage-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (TextMessage-builder-channel_id-set! b0.0 b1.1))
  (define (clear-TextMessage-builder-tree_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (TextMessage-builder-tree_id-set! b0.0 '()))
  (define (set-TextMessage-builder-tree_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (TextMessage-builder-tree_id-set! b0.0 b1.1))
  (define (clear-TextMessage-builder-message! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (TextMessage-builder-message-set! b0.0 ""))
  (define (has-TextMessage-builder-message? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-TextMessage-builder-message! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (TextMessage-builder-message-set! b0.0 b1.1))
  (define-record-type (TextMessage-builder
                        make-TextMessage-builder
                        TextMessage-builder?)
    (fields
      (mutable
        actor
        TextMessage-builder-actor
        TextMessage-builder-actor-set!)
      (mutable
        session
        TextMessage-builder-session
        TextMessage-builder-session-set!)
      (mutable
        channel_id
        TextMessage-builder-channel_id
        TextMessage-builder-channel_id-set!)
      (mutable
        tree_id
        TextMessage-builder-tree_id
        TextMessage-builder-tree_id-set!)
      (mutable
        message
        TextMessage-builder-message
        TextMessage-builder-message-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "actor"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "session"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 3 "channel_id"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 4 "tree_id"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 5 "message"
                protobuf:field-type-string #f #t "")))
          (let ([b3.3 (b1.1
                        (record-type-descriptor TextMessage)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (TextMessage-read r0.4)
    (protobuf:message-read (make-TextMessage-builder) r0.4))
  (define (TextMessage-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-TextMessage-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor TextMessage)
      e1.1))
  (define (TextMessage-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor TextMessage)
      e1.1))
  (define (has-TextMessage-actor? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define-record-type TextMessage
    (fields (immutable actor TextMessage-actor)
      (immutable session TextMessage-session)
      (immutable channel_id TextMessage-channel_id)
      (immutable tree_id TextMessage-tree_id)
      (immutable message TextMessage-message))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto BanList)
  (export BanList-BanEntry-builder-address
   set-BanList-BanEntry-builder-address!
   has-BanList-BanEntry-builder-address?
   clear-BanList-BanEntry-builder-address!
   BanList-BanEntry-builder-mask
   set-BanList-BanEntry-builder-mask!
   has-BanList-BanEntry-builder-mask?
   clear-BanList-BanEntry-builder-mask!
   BanList-BanEntry-builder-name
   set-BanList-BanEntry-builder-name!
   has-BanList-BanEntry-builder-name?
   clear-BanList-BanEntry-builder-name!
   BanList-BanEntry-builder-hash
   set-BanList-BanEntry-builder-hash!
   has-BanList-BanEntry-builder-hash?
   clear-BanList-BanEntry-builder-hash!
   BanList-BanEntry-builder-reason
   set-BanList-BanEntry-builder-reason!
   has-BanList-BanEntry-builder-reason?
   clear-BanList-BanEntry-builder-reason!
   BanList-BanEntry-builder-start
   set-BanList-BanEntry-builder-start!
   has-BanList-BanEntry-builder-start?
   clear-BanList-BanEntry-builder-start!
   BanList-BanEntry-builder-duration
   set-BanList-BanEntry-builder-duration!
   has-BanList-BanEntry-builder-duration?
   clear-BanList-BanEntry-builder-duration!
   clear-BanList-BanEntry-builder-extension!
   has-BanList-BanEntry-builder-extension?
   set-BanList-BanEntry-builder-extension!
   BanList-BanEntry-builder-extension
   BanList-BanEntry-builder-build BanList-BanEntry-builder?
   make-BanList-BanEntry-builder BanList-BanEntry-address
   BanList-BanEntry-mask BanList-BanEntry-name
   has-BanList-BanEntry-name? BanList-BanEntry-hash
   has-BanList-BanEntry-hash? BanList-BanEntry-reason
   has-BanList-BanEntry-reason? BanList-BanEntry-start
   has-BanList-BanEntry-start? BanList-BanEntry-duration
   has-BanList-BanEntry-duration?
   has-BanList-BanEntry-extension? BanList-BanEntry-extension
   BanList-BanEntry-read BanList-BanEntry-write
   BanList-BanEntry? BanList-builder-bans
   set-BanList-builder-bans! clear-BanList-builder-bans!
   BanList-builder-query set-BanList-builder-query!
   has-BanList-builder-query? clear-BanList-builder-query!
   clear-BanList-builder-extension!
   has-BanList-builder-extension?
   set-BanList-builder-extension! BanList-builder-extension
   BanList-builder-build BanList-builder? make-BanList-builder
   BanList-bans BanList-query has-BanList-query?
   has-BanList-extension? BanList-extension BanList-read
   BanList-write BanList?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (BanList-BanEntry-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-BanList-BanEntry-builder-extension! b0.0
           b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-BanList-BanEntry-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-BanList-BanEntry-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (BanList-BanEntry-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-BanList-BanEntry-builder-address! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (BanList-BanEntry-builder-address-set! b0.0 #vu8()))
  (define (has-BanList-BanEntry-builder-address? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-BanList-BanEntry-builder-address! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (BanList-BanEntry-builder-address-set! b0.0 b1.1))
  (define (clear-BanList-BanEntry-builder-mask! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (BanList-BanEntry-builder-mask-set! b0.0 0))
  (define (has-BanList-BanEntry-builder-mask? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-BanList-BanEntry-builder-mask! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (BanList-BanEntry-builder-mask-set! b0.0 b1.1))
  (define (clear-BanList-BanEntry-builder-name! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (BanList-BanEntry-builder-name-set! b0.0 ""))
  (define (has-BanList-BanEntry-builder-name? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-BanList-BanEntry-builder-name! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (BanList-BanEntry-builder-name-set! b0.0 b1.1))
  (define (clear-BanList-BanEntry-builder-hash! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (BanList-BanEntry-builder-hash-set! b0.0 ""))
  (define (has-BanList-BanEntry-builder-hash? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-BanList-BanEntry-builder-hash! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (BanList-BanEntry-builder-hash-set! b0.0 b1.1))
  (define (clear-BanList-BanEntry-builder-reason! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (BanList-BanEntry-builder-reason-set! b0.0 ""))
  (define (has-BanList-BanEntry-builder-reason? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-BanList-BanEntry-builder-reason! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (BanList-BanEntry-builder-reason-set! b0.0 b1.1))
  (define (clear-BanList-BanEntry-builder-start! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (BanList-BanEntry-builder-start-set! b0.0 ""))
  (define (has-BanList-BanEntry-builder-start? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 6)))
  (define (set-BanList-BanEntry-builder-start! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (BanList-BanEntry-builder-start-set! b0.0 b1.1))
  (define (clear-BanList-BanEntry-builder-duration! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 7))
    (BanList-BanEntry-builder-duration-set! b0.0 0))
  (define (has-BanList-BanEntry-builder-duration? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 7)))
  (define (set-BanList-BanEntry-builder-duration! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 7)
      b1.1)
    (BanList-BanEntry-builder-duration-set! b0.0 b1.1))
  (define-record-type (BanList-BanEntry-builder
                        make-BanList-BanEntry-builder
                        BanList-BanEntry-builder?)
    (fields
      (mutable
        address
        BanList-BanEntry-builder-address
        BanList-BanEntry-builder-address-set!)
      (mutable
        mask
        BanList-BanEntry-builder-mask
        BanList-BanEntry-builder-mask-set!)
      (mutable
        name
        BanList-BanEntry-builder-name
        BanList-BanEntry-builder-name-set!)
      (mutable
        hash
        BanList-BanEntry-builder-hash
        BanList-BanEntry-builder-hash-set!)
      (mutable
        reason
        BanList-BanEntry-builder-reason
        BanList-BanEntry-builder-reason-set!)
      (mutable
        start
        BanList-BanEntry-builder-start
        BanList-BanEntry-builder-start-set!)
      (mutable
        duration
        BanList-BanEntry-builder-duration
        BanList-BanEntry-builder-duration-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "address"
                protobuf:field-type-bytes #f #t #vu8())
              (protobuf:make-field-descriptor 2 "mask"
                protobuf:field-type-uint32 #f #t 0)
              (protobuf:make-field-descriptor 3 "name"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "hash"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 5 "reason"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 6 "start"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 7 "duration"
                protobuf:field-type-uint32 #f #f 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor BanList-BanEntry)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (BanList-BanEntry-read r0.4)
    (protobuf:message-read
      (make-BanList-BanEntry-builder)
      r0.4))
  (define (BanList-BanEntry-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-BanList-BanEntry-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor BanList-BanEntry)
      e1.1))
  (define (BanList-BanEntry-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor BanList-BanEntry)
      e1.1))
  (define (has-BanList-BanEntry-name? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-BanList-BanEntry-hash? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-BanList-BanEntry-reason? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-BanList-BanEntry-start? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 6)))
  (define (has-BanList-BanEntry-duration? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 7)))
  (define-record-type BanList-BanEntry
    (fields (immutable address BanList-BanEntry-address)
      (immutable mask BanList-BanEntry-mask)
      (immutable name BanList-BanEntry-name)
      (immutable hash BanList-BanEntry-hash)
      (immutable reason BanList-BanEntry-reason)
      (immutable start BanList-BanEntry-start)
      (immutable duration BanList-BanEntry-duration))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t))
  (define (BanList-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-BanList-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-BanList-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-BanList-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (BanList-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-BanList-builder-bans! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (BanList-builder-bans-set! b0.0 '()))
  (define (set-BanList-builder-bans! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (BanList-builder-bans-set! b0.0 b1.1))
  (define (clear-BanList-builder-query! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (BanList-builder-query-set! b0.0 #f))
  (define (has-BanList-builder-query? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-BanList-builder-query! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (BanList-builder-query-set! b0.0 b1.1))
  (define-record-type (BanList-builder
                        make-BanList-builder
                        BanList-builder?)
    (fields
      (mutable
        bans
        BanList-builder-bans
        BanList-builder-bans-set!)
      (mutable
        query
        BanList-builder-query
        BanList-builder-query-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "bans"
                (protobuf:make-message-field-type-descriptor "BanEntry" 'length-delimited protobuf:write-message
                  (lambda (p0.0)
                    (protobuf:message-read
                      (make-BanList-BanEntry-builder)
                      (open-bytevector-input-port
                        (get-bytevector-n
                          p0.0
                          (protobuf:read-varint p0.0)))))
                  BanList-BanEntry? #f #f)
                #t #f '())
              (protobuf:make-field-descriptor 2 "query"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1 (record-type-descriptor BanList) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (BanList-read r0.4)
    (protobuf:message-read (make-BanList-builder) r0.4))
  (define (BanList-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-BanList-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor BanList)
      e1.1))
  (define (BanList-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor BanList)
      e1.1))
  (define (has-BanList-query? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define-record-type BanList
    (fields
      (immutable bans BanList-bans)
      (immutable query BanList-query))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto UserState)
  (export UserState-builder-session
   set-UserState-builder-session!
   has-UserState-builder-session?
   clear-UserState-builder-session! UserState-builder-actor
   set-UserState-builder-actor! has-UserState-builder-actor?
   clear-UserState-builder-actor! UserState-builder-name
   set-UserState-builder-name! has-UserState-builder-name?
   clear-UserState-builder-name! UserState-builder-user_id
   set-UserState-builder-user_id!
   has-UserState-builder-user_id?
   clear-UserState-builder-user_id!
   UserState-builder-channel_id
   set-UserState-builder-channel_id!
   has-UserState-builder-channel_id?
   clear-UserState-builder-channel_id! UserState-builder-mute
   set-UserState-builder-mute! has-UserState-builder-mute?
   clear-UserState-builder-mute! UserState-builder-deaf
   set-UserState-builder-deaf! has-UserState-builder-deaf?
   clear-UserState-builder-deaf! UserState-builder-suppress
   set-UserState-builder-suppress!
   has-UserState-builder-suppress?
   clear-UserState-builder-suppress!
   UserState-builder-self_mute set-UserState-builder-self_mute!
   has-UserState-builder-self_mute?
   clear-UserState-builder-self_mute!
   UserState-builder-self_deaf set-UserState-builder-self_deaf!
   has-UserState-builder-self_deaf?
   clear-UserState-builder-self_deaf! UserState-builder-texture
   set-UserState-builder-texture!
   has-UserState-builder-texture?
   clear-UserState-builder-texture!
   UserState-builder-plugin_context
   set-UserState-builder-plugin_context!
   has-UserState-builder-plugin_context?
   clear-UserState-builder-plugin_context!
   UserState-builder-plugin_identity
   set-UserState-builder-plugin_identity!
   has-UserState-builder-plugin_identity?
   clear-UserState-builder-plugin_identity!
   UserState-builder-comment set-UserState-builder-comment!
   has-UserState-builder-comment?
   clear-UserState-builder-comment! UserState-builder-hash
   set-UserState-builder-hash! has-UserState-builder-hash?
   clear-UserState-builder-hash! UserState-builder-comment_hash
   set-UserState-builder-comment_hash!
   has-UserState-builder-comment_hash?
   clear-UserState-builder-comment_hash!
   UserState-builder-texture_hash
   set-UserState-builder-texture_hash!
   has-UserState-builder-texture_hash?
   clear-UserState-builder-texture_hash!
   UserState-builder-priority_speaker
   set-UserState-builder-priority_speaker!
   has-UserState-builder-priority_speaker?
   clear-UserState-builder-priority_speaker!
   UserState-builder-recording set-UserState-builder-recording!
   has-UserState-builder-recording?
   clear-UserState-builder-recording!
   clear-UserState-builder-extension!
   has-UserState-builder-extension?
   set-UserState-builder-extension! UserState-builder-extension
   UserState-builder-build UserState-builder?
   make-UserState-builder UserState-session
   has-UserState-session? UserState-actor has-UserState-actor?
   UserState-name has-UserState-name? UserState-user_id
   has-UserState-user_id? UserState-channel_id
   has-UserState-channel_id? UserState-mute has-UserState-mute?
   UserState-deaf has-UserState-deaf? UserState-suppress
   has-UserState-suppress? UserState-self_mute
   has-UserState-self_mute? UserState-self_deaf
   has-UserState-self_deaf? UserState-texture
   has-UserState-texture? UserState-plugin_context
   has-UserState-plugin_context? UserState-plugin_identity
   has-UserState-plugin_identity? UserState-comment
   has-UserState-comment? UserState-hash has-UserState-hash?
   UserState-comment_hash has-UserState-comment_hash?
   UserState-texture_hash has-UserState-texture_hash?
   UserState-priority_speaker has-UserState-priority_speaker?
   UserState-recording has-UserState-recording?
   has-UserState-extension? UserState-extension UserState-read
   UserState-write UserState?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (UserState-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-UserState-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-UserState-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-UserState-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (UserState-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-UserState-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (UserState-builder-session-set! b0.0 0))
  (define (has-UserState-builder-session? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-UserState-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (UserState-builder-session-set! b0.0 b1.1))
  (define (clear-UserState-builder-actor! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (UserState-builder-actor-set! b0.0 0))
  (define (has-UserState-builder-actor? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-UserState-builder-actor! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (UserState-builder-actor-set! b0.0 b1.1))
  (define (clear-UserState-builder-name! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (UserState-builder-name-set! b0.0 ""))
  (define (has-UserState-builder-name? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-UserState-builder-name! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (UserState-builder-name-set! b0.0 b1.1))
  (define (clear-UserState-builder-user_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (UserState-builder-user_id-set! b0.0 0))
  (define (has-UserState-builder-user_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-UserState-builder-user_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (UserState-builder-user_id-set! b0.0 b1.1))
  (define (clear-UserState-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (UserState-builder-channel_id-set! b0.0 0))
  (define (has-UserState-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-UserState-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (UserState-builder-channel_id-set! b0.0 b1.1))
  (define (clear-UserState-builder-mute! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (UserState-builder-mute-set! b0.0 #f))
  (define (has-UserState-builder-mute? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 6)))
  (define (set-UserState-builder-mute! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (UserState-builder-mute-set! b0.0 b1.1))
  (define (clear-UserState-builder-deaf! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 7))
    (UserState-builder-deaf-set! b0.0 #f))
  (define (has-UserState-builder-deaf? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 7)))
  (define (set-UserState-builder-deaf! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 7)
      b1.1)
    (UserState-builder-deaf-set! b0.0 b1.1))
  (define (clear-UserState-builder-suppress! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 8))
    (UserState-builder-suppress-set! b0.0 #f))
  (define (has-UserState-builder-suppress? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 8)))
  (define (set-UserState-builder-suppress! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 8)
      b1.1)
    (UserState-builder-suppress-set! b0.0 b1.1))
  (define (clear-UserState-builder-self_mute! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 9))
    (UserState-builder-self_mute-set! b0.0 #f))
  (define (has-UserState-builder-self_mute? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 9)))
  (define (set-UserState-builder-self_mute! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 9)
      b1.1)
    (UserState-builder-self_mute-set! b0.0 b1.1))
  (define (clear-UserState-builder-self_deaf! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 10))
    (UserState-builder-self_deaf-set! b0.0 #f))
  (define (has-UserState-builder-self_deaf? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 10)))
  (define (set-UserState-builder-self_deaf! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 10)
      b1.1)
    (UserState-builder-self_deaf-set! b0.0 b1.1))
  (define (clear-UserState-builder-texture! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 11))
    (UserState-builder-texture-set! b0.0 #vu8()))
  (define (has-UserState-builder-texture? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 11)))
  (define (set-UserState-builder-texture! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 11)
      b1.1)
    (UserState-builder-texture-set! b0.0 b1.1))
  (define (clear-UserState-builder-plugin_context! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 12))
    (UserState-builder-plugin_context-set! b0.0 #vu8()))
  (define (has-UserState-builder-plugin_context? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 12)))
  (define (set-UserState-builder-plugin_context! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 12)
      b1.1)
    (UserState-builder-plugin_context-set! b0.0 b1.1))
  (define (clear-UserState-builder-plugin_identity! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 13))
    (UserState-builder-plugin_identity-set! b0.0 ""))
  (define (has-UserState-builder-plugin_identity? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 13)))
  (define (set-UserState-builder-plugin_identity! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 13)
      b1.1)
    (UserState-builder-plugin_identity-set! b0.0 b1.1))
  (define (clear-UserState-builder-comment! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 14))
    (UserState-builder-comment-set! b0.0 ""))
  (define (has-UserState-builder-comment? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 14)))
  (define (set-UserState-builder-comment! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 14)
      b1.1)
    (UserState-builder-comment-set! b0.0 b1.1))
  (define (clear-UserState-builder-hash! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 15))
    (UserState-builder-hash-set! b0.0 ""))
  (define (has-UserState-builder-hash? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 15)))
  (define (set-UserState-builder-hash! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 15)
      b1.1)
    (UserState-builder-hash-set! b0.0 b1.1))
  (define (clear-UserState-builder-comment_hash! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 16))
    (UserState-builder-comment_hash-set! b0.0 #vu8()))
  (define (has-UserState-builder-comment_hash? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 16)))
  (define (set-UserState-builder-comment_hash! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 16)
      b1.1)
    (UserState-builder-comment_hash-set! b0.0 b1.1))
  (define (clear-UserState-builder-texture_hash! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 17))
    (UserState-builder-texture_hash-set! b0.0 #vu8()))
  (define (has-UserState-builder-texture_hash? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 17)))
  (define (set-UserState-builder-texture_hash! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 17)
      b1.1)
    (UserState-builder-texture_hash-set! b0.0 b1.1))
  (define (clear-UserState-builder-priority_speaker! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 18))
    (UserState-builder-priority_speaker-set! b0.0 #f))
  (define (has-UserState-builder-priority_speaker? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 18)))
  (define (set-UserState-builder-priority_speaker! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 18)
      b1.1)
    (UserState-builder-priority_speaker-set! b0.0 b1.1))
  (define (clear-UserState-builder-recording! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 19))
    (UserState-builder-recording-set! b0.0 #f))
  (define (has-UserState-builder-recording? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 19)))
  (define (set-UserState-builder-recording! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 19)
      b1.1)
    (UserState-builder-recording-set! b0.0 b1.1))
  (define-record-type (UserState-builder
                        make-UserState-builder
                        UserState-builder?)
    (fields
      (mutable
        session
        UserState-builder-session
        UserState-builder-session-set!)
      (mutable
        actor
        UserState-builder-actor
        UserState-builder-actor-set!)
      (mutable
        name
        UserState-builder-name
        UserState-builder-name-set!)
      (mutable
        user_id
        UserState-builder-user_id
        UserState-builder-user_id-set!)
      (mutable
        channel_id
        UserState-builder-channel_id
        UserState-builder-channel_id-set!)
      (mutable
        mute
        UserState-builder-mute
        UserState-builder-mute-set!)
      (mutable
        deaf
        UserState-builder-deaf
        UserState-builder-deaf-set!)
      (mutable
        suppress
        UserState-builder-suppress
        UserState-builder-suppress-set!)
      (mutable
        self_mute
        UserState-builder-self_mute
        UserState-builder-self_mute-set!)
      (mutable
        self_deaf
        UserState-builder-self_deaf
        UserState-builder-self_deaf-set!)
      (mutable
        texture
        UserState-builder-texture
        UserState-builder-texture-set!)
      (mutable
        plugin_context
        UserState-builder-plugin_context
        UserState-builder-plugin_context-set!)
      (mutable
        plugin_identity
        UserState-builder-plugin_identity
        UserState-builder-plugin_identity-set!)
      (mutable
        comment
        UserState-builder-comment
        UserState-builder-comment-set!)
      (mutable
        hash
        UserState-builder-hash
        UserState-builder-hash-set!)
      (mutable
        comment_hash
        UserState-builder-comment_hash
        UserState-builder-comment_hash-set!)
      (mutable
        texture_hash
        UserState-builder-texture_hash
        UserState-builder-texture_hash-set!)
      (mutable
        priority_speaker
        UserState-builder-priority_speaker
        UserState-builder-priority_speaker-set!)
      (mutable
        recording
        UserState-builder-recording
        UserState-builder-recording-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "session"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "actor"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "name"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "user_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 5 "channel_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 6 "mute"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 7 "deaf"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 8 "suppress"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 9 "self_mute"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 10 "self_deaf"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 11 "texture"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 12 "plugin_context"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 13 "plugin_identity"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 14 "comment"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 15 "hash"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 16 "comment_hash"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 17 "texture_hash"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 18 "priority_speaker"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 19 "recording"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1 (record-type-descriptor UserState) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (UserState-read r0.4)
    (protobuf:message-read (make-UserState-builder) r0.4))
  (define (UserState-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-UserState-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor UserState)
      e1.1))
  (define (UserState-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor UserState)
      e1.1))
  (define (has-UserState-session? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-UserState-actor? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-UserState-name? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-UserState-user_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-UserState-channel_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-UserState-mute? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 6)))
  (define (has-UserState-deaf? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 7)))
  (define (has-UserState-suppress? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 8)))
  (define (has-UserState-self_mute? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 9)))
  (define (has-UserState-self_deaf? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 10)))
  (define (has-UserState-texture? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 11)))
  (define (has-UserState-plugin_context? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 12)))
  (define (has-UserState-plugin_identity? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 13)))
  (define (has-UserState-comment? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 14)))
  (define (has-UserState-hash? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 15)))
  (define (has-UserState-comment_hash? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 16)))
  (define (has-UserState-texture_hash? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 17)))
  (define (has-UserState-priority_speaker? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 18)))
  (define (has-UserState-recording? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 19)))
  (define-record-type UserState
    (fields (immutable session UserState-session)
      (immutable actor UserState-actor)
      (immutable name UserState-name)
      (immutable user_id UserState-user_id)
      (immutable channel_id UserState-channel_id)
      (immutable mute UserState-mute)
      (immutable deaf UserState-deaf)
      (immutable suppress UserState-suppress)
      (immutable self_mute UserState-self_mute)
      (immutable self_deaf UserState-self_deaf)
      (immutable texture UserState-texture)
      (immutable plugin_context UserState-plugin_context)
      (immutable plugin_identity UserState-plugin_identity)
      (immutable comment UserState-comment)
      (immutable hash UserState-hash)
      (immutable comment_hash UserState-comment_hash)
      (immutable texture_hash UserState-texture_hash)
      (immutable priority_speaker UserState-priority_speaker)
      (immutable recording UserState-recording))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto UserRemove)
  (export UserRemove-builder-session
   set-UserRemove-builder-session!
   has-UserRemove-builder-session?
   clear-UserRemove-builder-session! UserRemove-builder-actor
   set-UserRemove-builder-actor! has-UserRemove-builder-actor?
   clear-UserRemove-builder-actor! UserRemove-builder-reason
   set-UserRemove-builder-reason!
   has-UserRemove-builder-reason?
   clear-UserRemove-builder-reason! UserRemove-builder-ban
   set-UserRemove-builder-ban! has-UserRemove-builder-ban?
   clear-UserRemove-builder-ban!
   clear-UserRemove-builder-extension!
   has-UserRemove-builder-extension?
   set-UserRemove-builder-extension!
   UserRemove-builder-extension UserRemove-builder-build
   UserRemove-builder? make-UserRemove-builder
   UserRemove-session UserRemove-actor has-UserRemove-actor?
   UserRemove-reason has-UserRemove-reason? UserRemove-ban
   has-UserRemove-ban? has-UserRemove-extension?
   UserRemove-extension UserRemove-read UserRemove-write
   UserRemove?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (UserRemove-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-UserRemove-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-UserRemove-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-UserRemove-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (UserRemove-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-UserRemove-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (UserRemove-builder-session-set! b0.0 0))
  (define (has-UserRemove-builder-session? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-UserRemove-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (UserRemove-builder-session-set! b0.0 b1.1))
  (define (clear-UserRemove-builder-actor! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (UserRemove-builder-actor-set! b0.0 0))
  (define (has-UserRemove-builder-actor? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-UserRemove-builder-actor! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (UserRemove-builder-actor-set! b0.0 b1.1))
  (define (clear-UserRemove-builder-reason! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (UserRemove-builder-reason-set! b0.0 ""))
  (define (has-UserRemove-builder-reason? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-UserRemove-builder-reason! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (UserRemove-builder-reason-set! b0.0 b1.1))
  (define (clear-UserRemove-builder-ban! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (UserRemove-builder-ban-set! b0.0 #f))
  (define (has-UserRemove-builder-ban? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-UserRemove-builder-ban! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (UserRemove-builder-ban-set! b0.0 b1.1))
  (define-record-type (UserRemove-builder
                        make-UserRemove-builder
                        UserRemove-builder?)
    (fields
      (mutable
        session
        UserRemove-builder-session
        UserRemove-builder-session-set!)
      (mutable
        actor
        UserRemove-builder-actor
        UserRemove-builder-actor-set!)
      (mutable
        reason
        UserRemove-builder-reason
        UserRemove-builder-reason-set!)
      (mutable
        ban
        UserRemove-builder-ban
        UserRemove-builder-ban-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "session"
                protobuf:field-type-uint32 #f #t 0)
              (protobuf:make-field-descriptor 2 "actor"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "reason"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "ban"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor UserRemove)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (UserRemove-read r0.4)
    (protobuf:message-read (make-UserRemove-builder) r0.4))
  (define (UserRemove-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-UserRemove-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor UserRemove)
      e1.1))
  (define (UserRemove-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor UserRemove)
      e1.1))
  (define (has-UserRemove-actor? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-UserRemove-reason? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-UserRemove-ban? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type UserRemove
    (fields
      (immutable session UserRemove-session)
      (immutable actor UserRemove-actor)
      (immutable reason UserRemove-reason)
      (immutable ban UserRemove-ban))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto ChannelState)
  (export ChannelState-builder-channel_id
   set-ChannelState-builder-channel_id!
   has-ChannelState-builder-channel_id?
   clear-ChannelState-builder-channel_id!
   ChannelState-builder-parent set-ChannelState-builder-parent!
   has-ChannelState-builder-parent?
   clear-ChannelState-builder-parent! ChannelState-builder-name
   set-ChannelState-builder-name!
   has-ChannelState-builder-name?
   clear-ChannelState-builder-name! ChannelState-builder-links
   set-ChannelState-builder-links!
   clear-ChannelState-builder-links!
   ChannelState-builder-description
   set-ChannelState-builder-description!
   has-ChannelState-builder-description?
   clear-ChannelState-builder-description!
   ChannelState-builder-links_add
   set-ChannelState-builder-links_add!
   clear-ChannelState-builder-links_add!
   ChannelState-builder-links_remove
   set-ChannelState-builder-links_remove!
   clear-ChannelState-builder-links_remove!
   ChannelState-builder-temporary
   set-ChannelState-builder-temporary!
   has-ChannelState-builder-temporary?
   clear-ChannelState-builder-temporary!
   ChannelState-builder-position
   set-ChannelState-builder-position!
   has-ChannelState-builder-position?
   clear-ChannelState-builder-position!
   ChannelState-builder-description_hash
   set-ChannelState-builder-description_hash!
   has-ChannelState-builder-description_hash?
   clear-ChannelState-builder-description_hash!
   ChannelState-builder-max_users
   set-ChannelState-builder-max_users!
   has-ChannelState-builder-max_users?
   clear-ChannelState-builder-max_users!
   clear-ChannelState-builder-extension!
   has-ChannelState-builder-extension?
   set-ChannelState-builder-extension!
   ChannelState-builder-extension ChannelState-builder-build
   ChannelState-builder? make-ChannelState-builder
   ChannelState-channel_id has-ChannelState-channel_id?
   ChannelState-parent has-ChannelState-parent?
   ChannelState-name has-ChannelState-name? ChannelState-links
   ChannelState-description has-ChannelState-description?
   ChannelState-links_add ChannelState-links_remove
   ChannelState-temporary has-ChannelState-temporary?
   ChannelState-position has-ChannelState-position?
   ChannelState-description_hash
   has-ChannelState-description_hash? ChannelState-max_users
   has-ChannelState-max_users? has-ChannelState-extension?
   ChannelState-extension ChannelState-read ChannelState-write
   ChannelState?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (ChannelState-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ChannelState-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ChannelState-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ChannelState-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ChannelState-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ChannelState-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ChannelState-builder-channel_id-set! b0.0 0))
  (define (has-ChannelState-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ChannelState-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ChannelState-builder-channel_id-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-parent! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ChannelState-builder-parent-set! b0.0 0))
  (define (has-ChannelState-builder-parent? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ChannelState-builder-parent! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ChannelState-builder-parent-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-name! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ChannelState-builder-name-set! b0.0 ""))
  (define (has-ChannelState-builder-name? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-ChannelState-builder-name! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ChannelState-builder-name-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-links! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (ChannelState-builder-links-set! b0.0 '()))
  (define (set-ChannelState-builder-links! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (ChannelState-builder-links-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-description! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (ChannelState-builder-description-set! b0.0 ""))
  (define (has-ChannelState-builder-description? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-ChannelState-builder-description! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (ChannelState-builder-description-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-links_add! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (ChannelState-builder-links_add-set! b0.0 '()))
  (define (set-ChannelState-builder-links_add! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (ChannelState-builder-links_add-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-links_remove! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 7))
    (ChannelState-builder-links_remove-set! b0.0 '()))
  (define (set-ChannelState-builder-links_remove! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 7)
      b1.1)
    (ChannelState-builder-links_remove-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-temporary! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 8))
    (ChannelState-builder-temporary-set! b0.0 #f))
  (define (has-ChannelState-builder-temporary? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 8)))
  (define (set-ChannelState-builder-temporary! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 8)
      b1.1)
    (ChannelState-builder-temporary-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-position! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 9))
    (ChannelState-builder-position-set! b0.0 0))
  (define (has-ChannelState-builder-position? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 9)))
  (define (set-ChannelState-builder-position! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 9)
      b1.1)
    (ChannelState-builder-position-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-description_hash! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 10))
    (ChannelState-builder-description_hash-set! b0.0 #vu8()))
  (define (has-ChannelState-builder-description_hash? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 10)))
  (define (set-ChannelState-builder-description_hash! b0.0
           b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 10)
      b1.1)
    (ChannelState-builder-description_hash-set! b0.0 b1.1))
  (define (clear-ChannelState-builder-max_users! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 11))
    (ChannelState-builder-max_users-set! b0.0 0))
  (define (has-ChannelState-builder-max_users? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 11)))
  (define (set-ChannelState-builder-max_users! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 11)
      b1.1)
    (ChannelState-builder-max_users-set! b0.0 b1.1))
  (define-record-type (ChannelState-builder
                        make-ChannelState-builder
                        ChannelState-builder?)
    (fields
      (mutable
        channel_id
        ChannelState-builder-channel_id
        ChannelState-builder-channel_id-set!)
      (mutable
        parent
        ChannelState-builder-parent
        ChannelState-builder-parent-set!)
      (mutable
        name
        ChannelState-builder-name
        ChannelState-builder-name-set!)
      (mutable
        links
        ChannelState-builder-links
        ChannelState-builder-links-set!)
      (mutable
        description
        ChannelState-builder-description
        ChannelState-builder-description-set!)
      (mutable
        links_add
        ChannelState-builder-links_add
        ChannelState-builder-links_add-set!)
      (mutable
        links_remove
        ChannelState-builder-links_remove
        ChannelState-builder-links_remove-set!)
      (mutable
        temporary
        ChannelState-builder-temporary
        ChannelState-builder-temporary-set!)
      (mutable
        position
        ChannelState-builder-position
        ChannelState-builder-position-set!)
      (mutable
        description_hash
        ChannelState-builder-description_hash
        ChannelState-builder-description_hash-set!)
      (mutable
        max_users
        ChannelState-builder-max_users
        ChannelState-builder-max_users-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "channel_id"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "parent"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "name"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "links"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 5 "description"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 6 "links_add"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 7 "links_remove"
                protobuf:field-type-uint32 #t #f '())
              (protobuf:make-field-descriptor 8 "temporary"
                protobuf:field-type-bool #f #f #f)
              (protobuf:make-field-descriptor 9 "position"
                protobuf:field-type-int32 #f #f 0)
              (protobuf:make-field-descriptor 10 "description_hash"
                protobuf:field-type-bytes #f #f #vu8())
              (protobuf:make-field-descriptor 11 "max_users"
                protobuf:field-type-uint32 #f #f 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ChannelState)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ChannelState-read r0.4)
    (protobuf:message-read (make-ChannelState-builder) r0.4))
  (define (ChannelState-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ChannelState-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ChannelState)
      e1.1))
  (define (ChannelState-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ChannelState)
      e1.1))
  (define (has-ChannelState-channel_id? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-ChannelState-parent? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-ChannelState-name? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-ChannelState-description? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-ChannelState-temporary? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 8)))
  (define (has-ChannelState-position? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 9)))
  (define (has-ChannelState-description_hash? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 10)))
  (define (has-ChannelState-max_users? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 11)))
  (define-record-type ChannelState
    (fields (immutable channel_id ChannelState-channel_id)
      (immutable parent ChannelState-parent)
      (immutable name ChannelState-name)
      (immutable links ChannelState-links)
      (immutable description ChannelState-description)
      (immutable links_add ChannelState-links_add)
      (immutable links_remove ChannelState-links_remove)
      (immutable temporary ChannelState-temporary)
      (immutable position ChannelState-position)
      (immutable description_hash ChannelState-description_hash)
      (immutable max_users ChannelState-max_users))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto ChannelRemove)
  (export ChannelRemove-builder-channel_id
    set-ChannelRemove-builder-channel_id!
    has-ChannelRemove-builder-channel_id?
    clear-ChannelRemove-builder-channel_id!
    clear-ChannelRemove-builder-extension!
    has-ChannelRemove-builder-extension?
    set-ChannelRemove-builder-extension!
    ChannelRemove-builder-extension ChannelRemove-builder-build
    ChannelRemove-builder? make-ChannelRemove-builder
    ChannelRemove-channel_id has-ChannelRemove-extension?
    ChannelRemove-extension ChannelRemove-read
    ChannelRemove-write ChannelRemove?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (ChannelRemove-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ChannelRemove-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ChannelRemove-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ChannelRemove-builder-extension! b0.0 b1.1
           b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ChannelRemove-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ChannelRemove-builder-channel_id! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ChannelRemove-builder-channel_id-set! b0.0 0))
  (define (has-ChannelRemove-builder-channel_id? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ChannelRemove-builder-channel_id! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ChannelRemove-builder-channel_id-set! b0.0 b1.1))
  (define-record-type (ChannelRemove-builder
                        make-ChannelRemove-builder
                        ChannelRemove-builder?)
    (fields
      (mutable
        channel_id
        ChannelRemove-builder-channel_id
        ChannelRemove-builder-channel_id-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "channel_id"
                protobuf:field-type-uint32 #f #t 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ChannelRemove)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ChannelRemove-read r0.4)
    (protobuf:message-read (make-ChannelRemove-builder) r0.4))
  (define (ChannelRemove-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ChannelRemove-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ChannelRemove)
      e1.1))
  (define (ChannelRemove-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ChannelRemove)
      e1.1))
  (define-record-type ChannelRemove
    (fields (immutable channel_id ChannelRemove-channel_id))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto ServerSync)
  (export ServerSync-builder-session
   set-ServerSync-builder-session!
   has-ServerSync-builder-session?
   clear-ServerSync-builder-session!
   ServerSync-builder-max_bandwidth
   set-ServerSync-builder-max_bandwidth!
   has-ServerSync-builder-max_bandwidth?
   clear-ServerSync-builder-max_bandwidth!
   ServerSync-builder-welcome_text
   set-ServerSync-builder-welcome_text!
   has-ServerSync-builder-welcome_text?
   clear-ServerSync-builder-welcome_text!
   ServerSync-builder-permissions
   set-ServerSync-builder-permissions!
   has-ServerSync-builder-permissions?
   clear-ServerSync-builder-permissions!
   clear-ServerSync-builder-extension!
   has-ServerSync-builder-extension?
   set-ServerSync-builder-extension!
   ServerSync-builder-extension ServerSync-builder-build
   ServerSync-builder? make-ServerSync-builder
   ServerSync-session has-ServerSync-session?
   ServerSync-max_bandwidth has-ServerSync-max_bandwidth?
   ServerSync-welcome_text has-ServerSync-welcome_text?
   ServerSync-permissions has-ServerSync-permissions?
   has-ServerSync-extension? ServerSync-extension
   ServerSync-read ServerSync-write ServerSync?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (ServerSync-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-ServerSync-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-ServerSync-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-ServerSync-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (ServerSync-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-ServerSync-builder-session! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (ServerSync-builder-session-set! b0.0 0))
  (define (has-ServerSync-builder-session? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-ServerSync-builder-session! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (ServerSync-builder-session-set! b0.0 b1.1))
  (define (clear-ServerSync-builder-max_bandwidth! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (ServerSync-builder-max_bandwidth-set! b0.0 0))
  (define (has-ServerSync-builder-max_bandwidth? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-ServerSync-builder-max_bandwidth! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (ServerSync-builder-max_bandwidth-set! b0.0 b1.1))
  (define (clear-ServerSync-builder-welcome_text! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (ServerSync-builder-welcome_text-set! b0.0 ""))
  (define (has-ServerSync-builder-welcome_text? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-ServerSync-builder-welcome_text! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (ServerSync-builder-welcome_text-set! b0.0 b1.1))
  (define (clear-ServerSync-builder-permissions! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (ServerSync-builder-permissions-set! b0.0 0))
  (define (has-ServerSync-builder-permissions? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-ServerSync-builder-permissions! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (ServerSync-builder-permissions-set! b0.0 b1.1))
  (define-record-type (ServerSync-builder
                        make-ServerSync-builder
                        ServerSync-builder?)
    (fields
      (mutable
        session
        ServerSync-builder-session
        ServerSync-builder-session-set!)
      (mutable
        max_bandwidth
        ServerSync-builder-max_bandwidth
        ServerSync-builder-max_bandwidth-set!)
      (mutable
        welcome_text
        ServerSync-builder-welcome_text
        ServerSync-builder-welcome_text-set!)
      (mutable
        permissions
        ServerSync-builder-permissions
        ServerSync-builder-permissions-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "session"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "max_bandwidth"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "welcome_text"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "permissions"
                protobuf:field-type-uint64 #f #f 0)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor ServerSync)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (ServerSync-read r0.4)
    (protobuf:message-read (make-ServerSync-builder) r0.4))
  (define (ServerSync-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-ServerSync-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor ServerSync)
      e1.1))
  (define (ServerSync-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor ServerSync)
      e1.1))
  (define (has-ServerSync-session? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-ServerSync-max_bandwidth? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-ServerSync-welcome_text? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-ServerSync-permissions? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type ServerSync
    (fields
      (immutable session ServerSync-session)
      (immutable max_bandwidth ServerSync-max_bandwidth)
      (immutable welcome_text ServerSync-welcome_text)
      (immutable permissions ServerSync-permissions))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto Reject)
  (export make-Reject-RejectType Reject-RejectType?
   Reject-RejectType Reject-builder-type
   set-Reject-builder-type! has-Reject-builder-type?
   clear-Reject-builder-type! Reject-builder-reason
   set-Reject-builder-reason! has-Reject-builder-reason?
   clear-Reject-builder-reason! clear-Reject-builder-extension!
   has-Reject-builder-extension? set-Reject-builder-extension!
   Reject-builder-extension Reject-builder-build
   Reject-builder? make-Reject-builder Reject-type
   has-Reject-type? Reject-reason has-Reject-reason?
   has-Reject-extension? Reject-extension Reject-read
   Reject-write Reject?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (Reject-RejectType? e0.0)
    (enum-set-member? e0.0 Reject-RejectType.1))
  (define Reject-RejectType.1
    (make-enumeration
      '(Reject-RejectType-AuthenticatorFail Reject-RejectType-NoCertificate
         Reject-RejectType-ServerFull Reject-RejectType-UsernameInUse
         Reject-RejectType-WrongServerPW
         Reject-RejectType-WrongUserPW
         Reject-RejectType-InvalidUsername
         Reject-RejectType-WrongVersion Reject-RejectType-None)))
  (define-enumeration Reject-RejectType
    (Reject-RejectType-AuthenticatorFail Reject-RejectType-NoCertificate
      Reject-RejectType-ServerFull Reject-RejectType-UsernameInUse
      Reject-RejectType-WrongServerPW
      Reject-RejectType-WrongUserPW
      Reject-RejectType-InvalidUsername
      Reject-RejectType-WrongVersion Reject-RejectType-None)
    make-Reject-RejectType)
  (define (Reject-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-Reject-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-Reject-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-Reject-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (Reject-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-Reject-builder-type! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (Reject-builder-type-set!
      b0.0
      (Reject-RejectType Reject-RejectType-AuthenticatorFail)))
  (define (has-Reject-builder-type? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-Reject-builder-type! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (Reject-builder-type-set! b0.0 b1.1))
  (define (clear-Reject-builder-reason! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (Reject-builder-reason-set! b0.0 ""))
  (define (has-Reject-builder-reason? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-Reject-builder-reason! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (Reject-builder-reason-set! b0.0 b1.1))
  (define-record-type (Reject-builder
                        make-Reject-builder
                        Reject-builder?)
    (fields
      (mutable type Reject-builder-type Reject-builder-type-set!)
      (mutable
        reason
        Reject-builder-reason
        Reject-builder-reason-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "type"
                (protobuf:make-enum-field-type-descriptor "RejectType" 'varint
                  (lambda (p0.0 p1.0)
                    (protobuf:write-varint
                      p0.0
                      (case p1.0
                        [(Reject-RejectType-AuthenticatorFail) 8]
                        [(Reject-RejectType-NoCertificate) 7]
                        [(Reject-RejectType-ServerFull) 6]
                        [(Reject-RejectType-UsernameInUse) 5]
                        [(Reject-RejectType-WrongServerPW) 4]
                        [(Reject-RejectType-WrongUserPW) 3]
                        [(Reject-RejectType-InvalidUsername) 2]
                        [(Reject-RejectType-WrongVersion) 1]
                        [(Reject-RejectType-None) 0])))
                  (lambda (p0.0)
                    (case (protobuf:read-varint p0.0)
                      [(8)
                       (Reject-RejectType
                         Reject-RejectType-AuthenticatorFail)]
                      [(7)
                       (Reject-RejectType Reject-RejectType-NoCertificate)]
                      [(6)
                       (Reject-RejectType Reject-RejectType-ServerFull)]
                      [(5)
                       (Reject-RejectType Reject-RejectType-UsernameInUse)]
                      [(4)
                       (Reject-RejectType Reject-RejectType-WrongServerPW)]
                      [(3)
                       (Reject-RejectType Reject-RejectType-WrongUserPW)]
                      [(2)
                       (Reject-RejectType
                         Reject-RejectType-InvalidUsername)]
                      [(1)
                       (Reject-RejectType Reject-RejectType-WrongVersion)]
                      [(0) (Reject-RejectType Reject-RejectType-None)]))
                  Reject-RejectType? #f #f)
                #f #f
                (Reject-RejectType Reject-RejectType-AuthenticatorFail))
              (protobuf:make-field-descriptor 2 "reason"
                protobuf:field-type-string #f #f "")))
          (let ([b3.3 (b1.1 (record-type-descriptor Reject) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (Reject-read r0.4)
    (protobuf:message-read (make-Reject-builder) r0.4))
  (define (Reject-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-Reject-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor Reject)
      e1.1))
  (define (Reject-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor Reject)
      e1.1))
  (define (has-Reject-type? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-Reject-reason? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define-record-type Reject
    (fields
      (immutable type Reject-type)
      (immutable reason Reject-reason))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto Ping)
  (export Ping-builder-timestamp set-Ping-builder-timestamp!
   has-Ping-builder-timestamp? clear-Ping-builder-timestamp!
   Ping-builder-good set-Ping-builder-good!
   has-Ping-builder-good? clear-Ping-builder-good!
   Ping-builder-late set-Ping-builder-late!
   has-Ping-builder-late? clear-Ping-builder-late!
   Ping-builder-lost set-Ping-builder-lost!
   has-Ping-builder-lost? clear-Ping-builder-lost!
   Ping-builder-resync set-Ping-builder-resync!
   has-Ping-builder-resync? clear-Ping-builder-resync!
   Ping-builder-udp_packets set-Ping-builder-udp_packets!
   has-Ping-builder-udp_packets?
   clear-Ping-builder-udp_packets! Ping-builder-tcp_packets
   set-Ping-builder-tcp_packets! has-Ping-builder-tcp_packets?
   clear-Ping-builder-tcp_packets! Ping-builder-udp_ping_avg
   set-Ping-builder-udp_ping_avg!
   has-Ping-builder-udp_ping_avg?
   clear-Ping-builder-udp_ping_avg! Ping-builder-udp_ping_var
   set-Ping-builder-udp_ping_var!
   has-Ping-builder-udp_ping_var?
   clear-Ping-builder-udp_ping_var! Ping-builder-tcp_ping_avg
   set-Ping-builder-tcp_ping_avg!
   has-Ping-builder-tcp_ping_avg?
   clear-Ping-builder-tcp_ping_avg! Ping-builder-tcp_ping_var
   set-Ping-builder-tcp_ping_var!
   has-Ping-builder-tcp_ping_var?
   clear-Ping-builder-tcp_ping_var!
   clear-Ping-builder-extension! has-Ping-builder-extension?
   set-Ping-builder-extension! Ping-builder-extension
   Ping-builder-build Ping-builder? make-Ping-builder
   Ping-timestamp has-Ping-timestamp? Ping-good has-Ping-good?
   Ping-late has-Ping-late? Ping-lost has-Ping-lost?
   Ping-resync has-Ping-resync? Ping-udp_packets
   has-Ping-udp_packets? Ping-tcp_packets has-Ping-tcp_packets?
   Ping-udp_ping_avg has-Ping-udp_ping_avg? Ping-udp_ping_var
   has-Ping-udp_ping_var? Ping-tcp_ping_avg
   has-Ping-tcp_ping_avg? Ping-tcp_ping_var
   has-Ping-tcp_ping_var? has-Ping-extension? Ping-extension
   Ping-read Ping-write Ping?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (Ping-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-Ping-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-Ping-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-Ping-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (Ping-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-Ping-builder-timestamp! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (Ping-builder-timestamp-set! b0.0 0))
  (define (has-Ping-builder-timestamp? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-Ping-builder-timestamp! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (Ping-builder-timestamp-set! b0.0 b1.1))
  (define (clear-Ping-builder-good! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (Ping-builder-good-set! b0.0 0))
  (define (has-Ping-builder-good? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-Ping-builder-good! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (Ping-builder-good-set! b0.0 b1.1))
  (define (clear-Ping-builder-late! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (Ping-builder-late-set! b0.0 0))
  (define (has-Ping-builder-late? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-Ping-builder-late! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (Ping-builder-late-set! b0.0 b1.1))
  (define (clear-Ping-builder-lost! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (Ping-builder-lost-set! b0.0 0))
  (define (has-Ping-builder-lost? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-Ping-builder-lost! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (Ping-builder-lost-set! b0.0 b1.1))
  (define (clear-Ping-builder-resync! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (Ping-builder-resync-set! b0.0 0))
  (define (has-Ping-builder-resync? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-Ping-builder-resync! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (Ping-builder-resync-set! b0.0 b1.1))
  (define (clear-Ping-builder-udp_packets! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 6))
    (Ping-builder-udp_packets-set! b0.0 0))
  (define (has-Ping-builder-udp_packets? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 6)))
  (define (set-Ping-builder-udp_packets! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 6)
      b1.1)
    (Ping-builder-udp_packets-set! b0.0 b1.1))
  (define (clear-Ping-builder-tcp_packets! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 7))
    (Ping-builder-tcp_packets-set! b0.0 0))
  (define (has-Ping-builder-tcp_packets? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 7)))
  (define (set-Ping-builder-tcp_packets! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 7)
      b1.1)
    (Ping-builder-tcp_packets-set! b0.0 b1.1))
  (define (clear-Ping-builder-udp_ping_avg! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 8))
    (Ping-builder-udp_ping_avg-set! b0.0 0))
  (define (has-Ping-builder-udp_ping_avg? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 8)))
  (define (set-Ping-builder-udp_ping_avg! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 8)
      b1.1)
    (Ping-builder-udp_ping_avg-set! b0.0 b1.1))
  (define (clear-Ping-builder-udp_ping_var! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 9))
    (Ping-builder-udp_ping_var-set! b0.0 0))
  (define (has-Ping-builder-udp_ping_var? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 9)))
  (define (set-Ping-builder-udp_ping_var! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 9)
      b1.1)
    (Ping-builder-udp_ping_var-set! b0.0 b1.1))
  (define (clear-Ping-builder-tcp_ping_avg! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 10))
    (Ping-builder-tcp_ping_avg-set! b0.0 0))
  (define (has-Ping-builder-tcp_ping_avg? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 10)))
  (define (set-Ping-builder-tcp_ping_avg! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 10)
      b1.1)
    (Ping-builder-tcp_ping_avg-set! b0.0 b1.1))
  (define (clear-Ping-builder-tcp_ping_var! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 11))
    (Ping-builder-tcp_ping_var-set! b0.0 0))
  (define (has-Ping-builder-tcp_ping_var? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 11)))
  (define (set-Ping-builder-tcp_ping_var! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 11)
      b1.1)
    (Ping-builder-tcp_ping_var-set! b0.0 b1.1))
  (define-record-type (Ping-builder
                        make-Ping-builder
                        Ping-builder?)
    (fields
      (mutable
        timestamp
        Ping-builder-timestamp
        Ping-builder-timestamp-set!)
      (mutable good Ping-builder-good Ping-builder-good-set!)
      (mutable late Ping-builder-late Ping-builder-late-set!)
      (mutable lost Ping-builder-lost Ping-builder-lost-set!)
      (mutable
        resync
        Ping-builder-resync
        Ping-builder-resync-set!)
      (mutable
        udp_packets
        Ping-builder-udp_packets
        Ping-builder-udp_packets-set!)
      (mutable
        tcp_packets
        Ping-builder-tcp_packets
        Ping-builder-tcp_packets-set!)
      (mutable
        udp_ping_avg
        Ping-builder-udp_ping_avg
        Ping-builder-udp_ping_avg-set!)
      (mutable
        udp_ping_var
        Ping-builder-udp_ping_var
        Ping-builder-udp_ping_var-set!)
      (mutable
        tcp_ping_avg
        Ping-builder-tcp_ping_avg
        Ping-builder-tcp_ping_avg-set!)
      (mutable
        tcp_ping_var
        Ping-builder-tcp_ping_var
        Ping-builder-tcp_ping_var-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "timestamp"
                protobuf:field-type-uint64 #f #f 0)
              (protobuf:make-field-descriptor 2 "good"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 3 "late"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 4 "lost"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 5 "resync"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 6 "udp_packets"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 7 "tcp_packets"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 8 "udp_ping_avg"
                protobuf:field-type-float #f #f 0)
              (protobuf:make-field-descriptor 9 "udp_ping_var"
                protobuf:field-type-float #f #f 0)
              (protobuf:make-field-descriptor 10 "tcp_ping_avg"
                protobuf:field-type-float #f #f 0)
              (protobuf:make-field-descriptor 11 "tcp_ping_var"
                protobuf:field-type-float #f #f 0)))
          (let ([b3.3 (b1.1 (record-type-descriptor Ping) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (Ping-read r0.4)
    (protobuf:message-read (make-Ping-builder) r0.4))
  (define (Ping-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-Ping-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor Ping)
      e1.1))
  (define (Ping-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor Ping)
      e1.1))
  (define (has-Ping-timestamp? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-Ping-good? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-Ping-late? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-Ping-lost? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define (has-Ping-resync? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define (has-Ping-udp_packets? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 6)))
  (define (has-Ping-tcp_packets? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 7)))
  (define (has-Ping-udp_ping_avg? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 8)))
  (define (has-Ping-udp_ping_var? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 9)))
  (define (has-Ping-tcp_ping_avg? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 10)))
  (define (has-Ping-tcp_ping_var? m0.0)
    (protobuf:field-has-value?
      (protobuf:message-field m0.0 11)))
  (define-record-type Ping
    (fields (immutable timestamp Ping-timestamp)
      (immutable good Ping-good) (immutable late Ping-late)
      (immutable lost Ping-lost) (immutable resync Ping-resync)
      (immutable udp_packets Ping-udp_packets)
      (immutable tcp_packets Ping-tcp_packets)
      (immutable udp_ping_avg Ping-udp_ping_avg)
      (immutable udp_ping_var Ping-udp_ping_var)
      (immutable tcp_ping_avg Ping-tcp_ping_avg)
      (immutable tcp_ping_var Ping-tcp_ping_var))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto Authenticate)
  (export Authenticate-builder-username
   set-Authenticate-builder-username!
   has-Authenticate-builder-username?
   clear-Authenticate-builder-username!
   Authenticate-builder-password
   set-Authenticate-builder-password!
   has-Authenticate-builder-password?
   clear-Authenticate-builder-password!
   Authenticate-builder-tokens set-Authenticate-builder-tokens!
   clear-Authenticate-builder-tokens!
   Authenticate-builder-celt_versions
   set-Authenticate-builder-celt_versions!
   clear-Authenticate-builder-celt_versions!
   Authenticate-builder-opus set-Authenticate-builder-opus!
   has-Authenticate-builder-opus?
   clear-Authenticate-builder-opus!
   clear-Authenticate-builder-extension!
   has-Authenticate-builder-extension?
   set-Authenticate-builder-extension!
   Authenticate-builder-extension Authenticate-builder-build
   Authenticate-builder? make-Authenticate-builder
   Authenticate-username has-Authenticate-username?
   Authenticate-password has-Authenticate-password?
   Authenticate-tokens Authenticate-celt_versions
   Authenticate-opus has-Authenticate-opus?
   has-Authenticate-extension? Authenticate-extension
   Authenticate-read Authenticate-write Authenticate?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (Authenticate-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-Authenticate-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-Authenticate-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-Authenticate-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (Authenticate-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-Authenticate-builder-username! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (Authenticate-builder-username-set! b0.0 ""))
  (define (has-Authenticate-builder-username? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-Authenticate-builder-username! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (Authenticate-builder-username-set! b0.0 b1.1))
  (define (clear-Authenticate-builder-password! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (Authenticate-builder-password-set! b0.0 ""))
  (define (has-Authenticate-builder-password? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-Authenticate-builder-password! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (Authenticate-builder-password-set! b0.0 b1.1))
  (define (clear-Authenticate-builder-tokens! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (Authenticate-builder-tokens-set! b0.0 '()))
  (define (set-Authenticate-builder-tokens! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (Authenticate-builder-tokens-set! b0.0 b1.1))
  (define (clear-Authenticate-builder-celt_versions! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (Authenticate-builder-celt_versions-set! b0.0 '()))
  (define (set-Authenticate-builder-celt_versions! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (Authenticate-builder-celt_versions-set! b0.0 b1.1))
  (define (clear-Authenticate-builder-opus! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 5))
    (Authenticate-builder-opus-set! b0.0 #f))
  (define (has-Authenticate-builder-opus? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 5)))
  (define (set-Authenticate-builder-opus! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 5)
      b1.1)
    (Authenticate-builder-opus-set! b0.0 b1.1))
  (define-record-type (Authenticate-builder
                        make-Authenticate-builder
                        Authenticate-builder?)
    (fields
      (mutable
        username
        Authenticate-builder-username
        Authenticate-builder-username-set!)
      (mutable
        password
        Authenticate-builder-password
        Authenticate-builder-password-set!)
      (mutable
        tokens
        Authenticate-builder-tokens
        Authenticate-builder-tokens-set!)
      (mutable
        celt_versions
        Authenticate-builder-celt_versions
        Authenticate-builder-celt_versions-set!)
      (mutable
        opus
        Authenticate-builder-opus
        Authenticate-builder-opus-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "username"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 2 "password"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 3 "tokens"
                protobuf:field-type-string #t #f '())
              (protobuf:make-field-descriptor 4 "celt_versions"
                protobuf:field-type-int32 #t #f '())
              (protobuf:make-field-descriptor 5 "opus"
                protobuf:field-type-bool #f #f #f)))
          (let ([b3.3 (b1.1
                        (record-type-descriptor Authenticate)
                        b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (Authenticate-read r0.4)
    (protobuf:message-read (make-Authenticate-builder) r0.4))
  (define (Authenticate-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-Authenticate-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor Authenticate)
      e1.1))
  (define (Authenticate-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor Authenticate)
      e1.1))
  (define (has-Authenticate-username? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-Authenticate-password? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-Authenticate-opus? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 5)))
  (define-record-type Authenticate
    (fields (immutable username Authenticate-username)
      (immutable password Authenticate-password)
      (immutable tokens Authenticate-tokens)
      (immutable celt_versions Authenticate-celt_versions)
      (immutable opus Authenticate-opus))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto UDPTunnel)
  (export UDPTunnel-builder-packet set-UDPTunnel-builder-packet!
    has-UDPTunnel-builder-packet?
    clear-UDPTunnel-builder-packet!
    clear-UDPTunnel-builder-extension!
    has-UDPTunnel-builder-extension?
    set-UDPTunnel-builder-extension! UDPTunnel-builder-extension
    UDPTunnel-builder-build UDPTunnel-builder?
    make-UDPTunnel-builder UDPTunnel-packet
    has-UDPTunnel-extension? UDPTunnel-extension UDPTunnel-read
    UDPTunnel-write UDPTunnel?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (UDPTunnel-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-UDPTunnel-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-UDPTunnel-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-UDPTunnel-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (UDPTunnel-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-UDPTunnel-builder-packet! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (UDPTunnel-builder-packet-set! b0.0 #vu8()))
  (define (has-UDPTunnel-builder-packet? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-UDPTunnel-builder-packet! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (UDPTunnel-builder-packet-set! b0.0 b1.1))
  (define-record-type (UDPTunnel-builder
                        make-UDPTunnel-builder
                        UDPTunnel-builder?)
    (fields
      (mutable
        packet
        UDPTunnel-builder-packet
        UDPTunnel-builder-packet-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "packet"
                protobuf:field-type-bytes #f #t #vu8())))
          (let ([b3.3 (b1.1 (record-type-descriptor UDPTunnel) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (UDPTunnel-read r0.4)
    (protobuf:message-read (make-UDPTunnel-builder) r0.4))
  (define (UDPTunnel-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-UDPTunnel-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor UDPTunnel)
      e1.1))
  (define (UDPTunnel-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor UDPTunnel)
      e1.1))
  (define-record-type UDPTunnel
    (fields (immutable packet UDPTunnel-packet))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
(library (MumbleProto Version)
  (export Version-builder-version set-Version-builder-version!
   has-Version-builder-version? clear-Version-builder-version!
   Version-builder-release set-Version-builder-release!
   has-Version-builder-release? clear-Version-builder-release!
   Version-builder-os set-Version-builder-os!
   has-Version-builder-os? clear-Version-builder-os!
   Version-builder-os_version set-Version-builder-os_version!
   has-Version-builder-os_version?
   clear-Version-builder-os_version!
   clear-Version-builder-extension!
   has-Version-builder-extension?
   set-Version-builder-extension! Version-builder-extension
   Version-builder-build Version-builder? make-Version-builder
   Version-version has-Version-version? Version-release
   has-Version-release? Version-os has-Version-os?
   Version-os_version has-Version-os_version?
   has-Version-extension? Version-extension Version-read
   Version-write Version?)
  (import (rnrs base) (rnrs enums) (rnrs io ports)
    (rnrs records syntactic) (protobuf private))
  (define (Version-builder-build b0.0)
    (protobuf:message-builder-build b0.0))
  (define (clear-Version-builder-extension! b0.0 b1.1)
    (protobuf:clear-message-builder-extension! b0.0 b1.1))
  (define (has-Version-builder-extension? b0.0 b1.1)
    (protobuf:message-builder-has-extension? b0.0 b1.1))
  (define (set-Version-builder-extension! b0.0 b1.1 b2.2)
    (protobuf:set-message-builder-extension! b0.0 b1.1 b2.2))
  (define (Version-builder-extension b0.0 b1.1)
    (protobuf:message-builder-extension b0.0 b1.1))
  (define (clear-Version-builder-version! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 1))
    (Version-builder-version-set! b0.0 0))
  (define (has-Version-builder-version? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 1)))
  (define (set-Version-builder-version! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 1)
      b1.1)
    (Version-builder-version-set! b0.0 b1.1))
  (define (clear-Version-builder-release! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 2))
    (Version-builder-release-set! b0.0 ""))
  (define (has-Version-builder-release? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 2)))
  (define (set-Version-builder-release! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 2)
      b1.1)
    (Version-builder-release-set! b0.0 b1.1))
  (define (clear-Version-builder-os! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 3))
    (Version-builder-os-set! b0.0 ""))
  (define (has-Version-builder-os? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 3)))
  (define (set-Version-builder-os! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 3)
      b1.1)
    (Version-builder-os-set! b0.0 b1.1))
  (define (clear-Version-builder-os_version! b0.0)
    (protobuf:clear-field!
      (protobuf:message-builder-field b0.0 4))
    (Version-builder-os_version-set! b0.0 ""))
  (define (has-Version-builder-os_version? b0.0)
    (protobuf:field-has-value?
      (protobuf:message-builder-field b0.0 4)))
  (define (set-Version-builder-os_version! b0.0 b1.1)
    (protobuf:set-field-value!
      (protobuf:message-builder-field b0.0 4)
      b1.1)
    (Version-builder-os_version-set! b0.0 b1.1))
  (define-record-type (Version-builder
                        make-Version-builder
                        Version-builder?)
    (fields
      (mutable
        version
        Version-builder-version
        Version-builder-version-set!)
      (mutable
        release
        Version-builder-release
        Version-builder-release-set!)
      (mutable os Version-builder-os Version-builder-os-set!)
      (mutable
        os_version
        Version-builder-os_version
        Version-builder-os_version-set!))
    (parent protobuf:message-builder)
    (protocol
      (lambda (b1.1)
        (lambda ()
          (define b2.2
            (list
              (protobuf:make-field-descriptor 1 "version"
                protobuf:field-type-uint32 #f #f 0)
              (protobuf:make-field-descriptor 2 "release"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 3 "os"
                protobuf:field-type-string #f #f "")
              (protobuf:make-field-descriptor 4 "os_version"
                protobuf:field-type-string #f #f "")))
          (let ([b3.3 (b1.1 (record-type-descriptor Version) b2.2)])
            (apply
              b3.3
              (map protobuf:field-descriptor-default b2.2))))))
    (sealed #t))
  (define (Version-read r0.4)
    (protobuf:message-read (make-Version-builder) r0.4))
  (define (Version-write w0.2 w1.3)
    (protobuf:message-write w0.2 w1.3))
  (define (has-Version-extension? e0.0 e1.1)
    (protobuf:message-has-extension?
      e0.0
      (record-type-descriptor Version)
      e1.1))
  (define (Version-extension e0.0 e1.1)
    (protobuf:message-extension
      e0.0
      (record-type-descriptor Version)
      e1.1))
  (define (has-Version-version? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 1)))
  (define (has-Version-release? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 2)))
  (define (has-Version-os? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 3)))
  (define (has-Version-os_version? m0.0)
    (protobuf:field-has-value? (protobuf:message-field m0.0 4)))
  (define-record-type Version
    (fields
      (immutable version Version-version)
      (immutable release Version-release)
      (immutable os Version-os)
      (immutable os_version Version-os_version))
    (opaque #t)
    (parent protobuf:message)
    (sealed #t)))
